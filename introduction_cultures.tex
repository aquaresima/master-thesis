\chapter{What are neuronal cultures and devices}
In this chapter we will move from the abstract description of network and computation in brain tissue towards an
effective investigation of computational abilities of cultured neurons. We are interested in understanding the
basic, minimal, properties of neuronal tissue, and we will concentrate our research on \textit{in vitro} systems. In
the following we will describe briefly what are neuronal cultures and how they can be arranged to form a functional
circuit.

Before to start, some remarks. Leaving well-behaved models and semi-conductors for the realm of biology is easier said than done. Using living
materials imposes a lot of constraints to the experimenter who wishes to build something out of them. As everything that
is alive can die, the first challenge is to keep these living materials healthy as long as necessary. Providing a
physiological environment with the appropriate nutrients is of course the main determinant for survival. Like
they do in vivo, cultured neurons are subjected to aging, giving as a consequence an additional layer of temporal
constraints. The development and maturation of cultured neuronal networks actually involves a large panel of processes
through which neurons first grow and establish synapses with other neurons, then express different sets of genes as time
goes, causing some major shifts in their electrophysiological properties.
Provided that the basic needs of neurons are satisfied, and that they are in the right developmental stage, there is
another hurdle: homeostasis. Cells that we grow in vitro have originally evolved different compensatory mechanisms to
maintain an internal state that guarantees proper operation across a wide range of environmental conditions, for the
sake of their common good, that is the survival of the animals they normally constitute. The cells will continue to
fiercely regulate themselves when grown in a Petri dish, whether or not the functional implications are relevant to the
experimenter. The sad corollary is that properties of interest that are incidentally connected to these homeostatic
mechanisms can hardly be controlled experimentally, at least under physiological conditions. The usage of microfludic
chamber soothe this issue.

A closer look to this experimental setup is offered in the next section.


\begin{figure}[p]
	\centering
        \includegraphics[width=0.8\textwidth]{introduction/culture}
	\caption{\textbf{Neuronal cultures}
                Neuronal cultures are
                an excellent tool to
                investigate arising
                neuronal networks and activity dynamics.
                Various types of
                measures can be
                obtained from the
                culture, from single
                cell activity up to a
                global picture of
                networks dynamics with
                Ca++ Imaging. The
                modern techs in soft
                lithography permit
                finely modeled
                structures, and
                microfluidic are used
                to feed the cells. Nevertheless the observation of
                activity \textit{in vitro} cannot be directly compared to \textit{in vivo} studies because the lack of
                sensory stimulus and the different homeostasis affect irreversibly the population activity.
                \textbf{a} Schematic representation of the culturing process by using pierced PDMS molds (blue) attached to glass coverslips (yellow).       The preparation process (top) included piercing, autoclaving and polylysine coating;
		the culturing process (center) yielded to the formation of mini–cultures in the pierced
		areas; the final process (bottom) consisted in the removal of the PDMS mold and
		the preparation of the culture for the measurements.\\
                \textbf{b} Actual image of combined glass–PDMS structure 11 days after plating. The rectangular area
                depicts the maximum field–of–view (FOV) of the camera ( $8.2$ x $6.1$ mm) used in the experiment.\\
                \textbf{c} Bright field image of a
		small region of a culture. Round objects are neurons’ cell bodies.~\cite{Orlandi2013a}
	}
	\label{fig:culture}

\end{figure}
\section{Basic description of cultures: fabrication, properties, applications}
A cultured neuronal network is a cell culture of neurons. It is used as a model to study the central nervous system or
to investigate fundamentals properties of the living matter. A scheme is depicted in Fig.~\ref{fig:culture}.
The fabrication stages are generally based on soft litography, which enables for resolution at tens of nanometers scale,
the most common material is the PDMS (Polydimethylsiloxane), an elastomeric organosilicon with notable visco-elastic
properties. Soft litography has a wide set of advantages over the traditional litography techniques and is widely used
in biotechnology. The properties of PDMS include the possibility to build sharp edge structures where neuron can be
confined.
Neuronal networks are typically cultured from dissociated rat neurons, because of their wide availability. Studies
commonly employ rat cortical, hippocampal, and spinal neurons although laboratory mouse neurons have also been used.
One of the most formidable problems associated with cultured neuronal networks is their lack of longevity. Like most cell cultures, neuron cultures are highly susceptible to infection. The long timelines associated with studying neuronal plasticity – usually on the scale of months – make the extension of the lifespan of neurons \textit{in vitro} paramount.
The applications of such devices range from medical to fundamental research. Starting from the laboratories which I personally
visited or acknowledged, they include basic studies on axon guidance and interaction with chemical and mechanical
environment \footnote{Villard, Ipgg lab, Paris \cite{Renault2016}}, graphene based neuronal tissue reconstruction or 3D growth from PDMS micro-pored
structures \footnote{Ballerini, Sissa, Trieste \cite{Bosi2015, Fabbro2012}}, and full network studies as those
performed on bursting spatial patterns \footnote{Soriano, UBV, Barcelona \cite{Orlandi2013a}}.
The flaws of neuronal cultures are numerous, not even counting the difference in terms of lifespan timescales. Cultured
neuronal networks are by definition disembodied. Therefore,
the neurons are influenced in ways that are not biologically normal when outside their natural environment. Among these abnormalities is the foremost fact that
the neurons are usually harvested as neural stem cells from a foetus and are therefore disrupted at a critical stage in
network development.
Lacking a body, neurons are also devoid of sensory inputs as
well of the ability to express behaviour – a crucial characteristic in learning and memory experiments. It is believed
that such sensory deprivation has adverse effects on the development of these cultures and may result in abnormal
patterns of behavior throughout the network.
Traditional cultured networks, 3D structure is a promising exception, are flat, single-layer sheets of cells with
connectivity only two dimensions. Most in vivo neuronal systems, to the contrary, are large three-dimensional structures
with much greater interconnectivity. This remains one of the most striking differences between the model and the
reality, and this fact probably plays a large role in skewing some of the conclusions derived from experiments based on
this model.\\
The spectrum of accessible experiments and application has increased with the introduction of more sophisticated
techniques of production. The Microfluidics has emerged as an important technology in academic research and even more
remarkably in the industry, to control the architecture of neural networks growth \textit{in vitro}.
Using microfluidic devices offers indeed many advantages over simple patterning techniques, including high
manufacturability, robustness, controllable environment, etc...\\
In the next section a basic idea of tools and methods used to study neuronal culture is presented, then some highlights
on the activity of the neuronal culture are reported and eventually the section is concluded with some notes on a particular type of culture: the one produce at IPGG and whom I will refer often throughout the manuscript.
%The connectivity in neuronal chips is usually controlled using microtunnels that physically allow axons to grow between different populations
%of neurons while keeping the cell somas in their respective compartments.
%However, using such topographical guidance becomes rapidly challenging
%when networks get larger and more complex. Since the range of applications
%of such chips is ultimately limited by the connectivity between the neuronal
%compartments, the development of technologies allowing to control axonal
%growth reliably is critical for future research.

\section{Instruments to observe and study neuronal cultures}
The investigation of neuronal cultures is accomplished with several techniques, whose perspectives and invasiveness change
appreciably. I had the chance to study and, in some case, perform some experiments during the Neuron Technology Summer School in
Trieste, two weeks of seminars and hands-on sessions focused on the experimental techniques of neurobiology.

Single neuron activity investigation can be conducted in the field of electrophysiology: measuring the electrical potential at
various position in the cell's shaft uncovers the main communication pathway for neuronal networks. Electrophysiology can access
the voltage profile of a single neuron, or larger areas field potentials. These experimental methods are the most long-lived in
experimental neurobiology and where fundamental for the formulation of neuronal dynamics models. The intrusiveness of
such experiments depends on the setup, but usually the single cell recordings lead the cell to death in the range of a few
hours.\\
A wider perspective can be obtained with the Multi Electrode Arrays (MEA), at the cost of less resolution. MEA are grid structures of
in/out electrodes. They allow for largely attenuated and temporally filtered signals as well as  simultaneous recordings of large
populations without cell labelling. The electrode diameter is 100 $\mu m$ and a standard component is Silver Chloride, which
converts ionic current into electricity.
Reversely, it is possible to electrically stimulate neurons through the electrodes.
MEA is not recording the internal properties but the local field potential (LFP). The LFP signal is blind to the subtreshold
area of the neuron activity, and EPSP or IPSP-released present the same LFPs .
Recent approaches consist in changing the shape of the array because the plate shape has a limited coupling efficiency.
When the morphology, the topology, or the overall network dynamics is the investigated property the electrophysiology
gives way to imaging techniques. The morphology, for instance, can be investigated by fluorescent microscopy, e.g.
Audric~\footnote{In the context of his
    Report des stage, which is not
available online} used \textit{SiR} fluorescent dymers to evaluate tubulin amounts in outgrowing neurons.
An exceptional investigation tool has recently been introduced with Calcium Imaging ~\cite{Grienberger2012}.
The Ca++ intracellular current occurring in spiking neurons proxies for a spatial localized pictures of activity. The
Calcium Imaging is widely used to test for pharmacological response, for bursting patterns, and for numerous other
experiments that associate single neuron labelling (i.e. by its position in the culture) to culture dynamics on very large scales. Calcium Imaging can be combined with MEA for high quality spatial profiling of culture electrical
response. There are severe flaws for this method too. First, the Ca++ marking dimer decays over time and poisons the
cell. Second, the measured signal is coarse in time and is attenuated on scales much longer than the neurons' spikes. The Calcium
signal cannot eventually distinguish a inhibitory signal from an excitatory one.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{introduction/mea}
    \caption{\textbf{MEA culture dish and close-up look on the electrodes}. The electro-stimulation of cultured neurons
        can be arranged as a learning
        protocol for the network. LTSP and LTSD (long term
            synaptic potentiation or
        depotentiation) can be triggered with external
        stimuli from the electrical grid
        placed at the bottom
        surface of the culture. These arrays are generally composed by tens of
        micrometers electrodes which can stimulate the tissue and track the response.\\
       The picture shows a weekold culture of circa $50000$ neurons and glia from embryonic rat cortex, growing in a MEA and forming a dense network 1-2 mm across.  Fifty-nine 30
        $\mu m$ electrodes spaced at 200 $\mu m$ intervals connect a few hundred of the network’s neurons to the outside
        world, by allowing their activity to be extracellularly recorded or evoked by electrical stimulation.}
    \label{fig:mea}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[width=\textwidth]{introduction/calcium}
    \caption {\textbf{High-contrast bright-field image of a neuronal culture in Ca++} Calcium Imaging permits to
        measure the spatial localization of tissue activity. Nonetheless the resolution is not at spikes' timescale the
        investigation of bursting properties can be effectively tracked. The culture depicted is grown on glass and confined within
        a circular diameter. The culture contains about $3000$ neurons.\\
        \textbf{b} Bright-field image showing a detail of
        the culture and the distribution of neurons. The circle identifies a single neuron.
        \textbf{c} Corresponding fluorescence image during a spontaneous activity event. Bright spots are firing
    neurons. The resolution of the image is the same as the actual measurements. \textbf{d} Fluorescence signal from a
30 minutes recording of the spontaneous activity in the culture shown in \textbf{a}, averaged over the 500 brightest neurons. The
top plot corresponds to measurements with both excitation and inhibition active (E+I network); the bottom one
corresponds to excitation-only measurements (E network), with inhibitory synapses blocked with 40 $\mu m$ Bicuculline.
Fluorescence peaks identify network bursts. The symbols below each burst identify its initiation in a specific area of
the culture.}
    \label{fig:calcium}
\end{figure}

\section{Activity in neuronal cultures}
When the culture is constructed and the neurons displaced on the Petri dish,
the complex process of growth takes place. This process itself is detailed in
the thesis and the scope of NetGrowth is to reproduce it.\\
The importance of
electrical activity in the shaping of the network topology is well assessed in the neurobiologist community, e.g.\
neurons die precociously when failing to connect, but this topic belongs to the future perspective of this research
project and will not be tackle. Here, we want to discuss the electrical activity that animates the mature culture, once the growth is coming to
an end and before their feeble life ceases. The literature on this topic is wide, for simplicity I will concentrate on the experiments the group
of J. Soriano is pursuing in his lab in Barcelona.\\
Once the most of synapses are established, and the topology becomes more constant in time, the culture enters in the
bursting phase. This phase is essentially dominated by the spreading of activation waves throughout the culture cell;
these waves can be observed, e.g, with Calcium Imaging.
The generation of these waves is investigated both theoretically~\cite{Fardet2018} and experimentally~\cite{Orlandi2013a}
and reveal many similarities with the physical phenomenon of coupled oscillators: a
sufficient condition for network bursting is the presence of an excitatory population of oscillatory neurons which
displays spike-driven adaptation, this result was proven on fully connected graph.
The experiment conducted at Soriano Lab, UVB, Barcelona reveals that the waves nucleate at specific sites of the culture
and propagate very rapidly (with an increasing speed during maturation), and then are followed by long periods of
silence or very low activity.  More importantly, these sites are specific (from culture to culture): there are preferred
zones in the system where waves nucleate, and these are not correlated with any local properties of the network (firing
rate, density, connectivity, clustering,
\ldots )\footnote{\href{http://javierorlandi.com/?page_id=283}{Javier Orland
		website}}.\\
To solve the puzzling phenomenon of bursting is an essential step towards the realization of neuronal device and, more
important, the understanding of neuronal encoding of information.  In this perspective the use of activity simulators, e.g.
NEST\footnote{http://www.nest-simulator.org}, assumes a fundamental role: it can help to test hypothesis and make
predictions. Whether the dynamics of fully connected graph can be easily performed the reproduction of a
realistic network has not be taken for granted, because the actual adjacency matrix of cultured network remains mostly
unknown. In the next chapter we will see the important role NetGrowth can fulfill in this context.

\section{Neural circuitry in Lab-On-A-Chip technologies}
In this section I will report the work done by R. Renault during his PhD in the MSC Lab, focusing on the studies and
experiments he accomplished towards the realization of an \textit{in vitro} functional cell, also said a Lab-On-a-Chip.\\
R.\ has focused on shaping the networks at larger scales, organizing the connectivity between groups of neurons instead
of guiding single neurons. The organization of neurons on this larger scale can be
controlled by direct physical confinement; this is generally achieved using micro-fluidics chips.
They consist in molded silicon polymer (polydimethylsiloxane or PDMS) covalently attached to glass coverslips through plasma-
activated bonding.
He proposed a new paradigm for neuronal devices:
Primarly, microwells were used to compartmentalize cultures into neuronal populations, individually
acting as super-neurons. These super-neurons are both robust and customizable as their properties emerge from the mean composition and density of
the seeded cell suspension, which can be controlled accurately. They are also
easy to read, as neuronal populations can display all-or-none bursts of activity
which are easily detectable. Even if the amplitude of such bursts can vary and
therefore carry information relevant to neuronal computation [58], they can
also be seen as stereotypical events corresponding to super-neuron spikes ; the
investigation of temporal codes is therefore straightforward in such scheme.
\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{introduction/diodes}
	\caption{\textbf{Minimal neuronal devices} Neuronal devices helps investigating spatial and time coding of
            neuronal networks. The paradigma proposed in \cite{Renault2016} uses large population as super neurons,
            whose activity pattern should be more reliable. Populations are connected with directional tunnels as those
            connecting the population
            in the pictures. These
            tunnels can be diode like
            or not, the axon is
            expected to walk up these
            structures looking for
            post-synaptic targets.\\
            \textbf{Top}: Two populations of circa 700
		fluorescent neurons are connected through micro-tunnels, the geometry of
		which can be controlled to yield unidirectional connectivity. Only a fraction of
		neurons are fluorescent here.
            \textbf{Bottom}: Three populations device (two inputs
		and one output) showing neurons stained for the cytoskeletal protein MAP2.
		Two and three populations devices constitute the building blocks of more
		elaborate devices.}
	\label{fig:diodes}
\end{figure}
Secondly, the populations are connected through arrays of axon-selective
micro-channels which control topographically the initial connectivity between
them. Since the directed connections in the network are spatially separated,
each can be reinforced individually without affecting the others.\\
This type of architecture can bypass the limitations of previous approaches where learning
is hindered by highly reciprocal and overlapping connections in the culture.
Additionally, network topologies developed in the field of ANNs can be easily
implemented using this approach (Fig.~\ref{fig:diodes}).
The fact that the network topology inside each population is not constrained in such devices means that a lot
of computational power is potentially lost. In the worst case scenario, and
provided that populations can actually act as super-neurons, the number of
useful computing units would be the number of populations, which is far
less than the actual number of neurons. However, random sub-circuits inside
each population could be exploited to multiplex different functions on each
unit, as it is done on unconstrained cultures growing onto MEA\@.
The computational capabilities of population units are therefore difficult to predict, but
they would at least support the properties of the units inside a perceptron.\\
The learning protocols, Renaud carried through, failed for most of the structures where neurons were cultured, but the idea stays and always more groups and projects are studying the possibility of complex and confined structures in cultures.
The costs of creating a functional cell, from the realization of the PDMS structure up to the learning protocol, unveil the remarkable necessity of creating a cheap test bench. The simulator is, in this case, becomes a suitable instrument to explore the fog and test culture shapes and learning protocol.
%In this frame the introduction of a neuronal growth
%simulator becomes a striking advantage for the researcher who can test culture architectures before investing time and
%mices in microfluidic experiments.
Furthermore in the context of axon guidance the availability of a modular tool, to implement and verify chemio-mechanical
interactions model, is very handy as well.
In the next section the simulation of culture is detailed, and some other cases which can benefit of NetGrowth simulator
are indicated.
