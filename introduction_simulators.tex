%% !TeX spellcheck = en_UK
%% !TeX root = intoduction_simulators.tex
%\documentclass[tesi.tex]{subfiles}
%
%\begin{document}
\chapter{\textit{In silico} neuronal cultures, applications and limits}
Whether the complexity of life beings, the multitudes of chemical reactions and
micro-physics process go far beyond the computational possibilities of modern
computers\footnote{It actually depends by the scale of the process we want to
	reproduce, indeed the folding of proteins can be simulated, either, but
	once at time and for just few seconds} the computers have revealed powerful
tools at the service of neuroscientists~\cite{amit1998}.
Nowadays each research lab has an IT infrastructure, many have
computing servers, and very often researchers are skilled in one or more
programming languages.
The possibilities opened by the spread of IT technologies are numerous:
\begin{itemize}
	\item Non analytical theoretical models can be implemented and checked,
	      e.g.\ simulate the propagation of an electrical signal on a complex
	      dendritic tree.
	\item The research can be performed on synthetic data, avoiding the complex and expensive infrastructure
	      needed to grow the neurons, nor the expertise to perform the experiments.
	\item The results become very reproducible and can be tested by the
	      scientific community. Other researchers can recreate the experiment and
	      adjoin new research pathways.
\end{itemize}
In this section we will zoom on the problem of reproducing neurons and neuronal
cultures with computers.
In the first section I will have a look to the perspectives and applications
of neuronal simulations, stressing their relation with
\textit{in vitro} experiments. Then a brief review of existent simulators will
be presented, focusing on those most related to the NetGrowth simulator.

\section{Why simulate neuronal growth}
There are many reasons why creating effective and realistic neuronal morphologies
can be interesting; they come from both neurobiology and theoretical neuroscience. This
double importance is the consequence of the many scales observed in
neuroscience. Let us introduce both these quests and look at how the
simulation of neuronal morphology can pave the way to a deeper understanding of
both neurobiological and cognitive processes. \\
Regarding the former, the growth of neurons, and the various morphologies they
assume, is a puzzling question for biologists and biophysicists. The mechanisms
that regulate the growth, both chemical and mechanical, are a subject of significant
interest and experimentation in modern research, as I had the opportunity to
understand during the Neuron Technology summer school. The study of the mechanical interactions
between neurons and substrate opens the doors to relevant medical applications,
e.g.\ in the field of rehabilitation. The resulting morphology sheds light on the
chemical process going on below the cell surface and accounts for the role of specific neurons in the population: its ability to integrate signals and
process information depends mainly from the dendritic tree.
All this variable are kept into account in biophysical models as those presented
in~\cite{Recho2016}, or~\cite{HELY2001}. Neuronal growth simulators offer the
opportunity to test these models and observe the consequences over the whole
morphology, opening the door to effective comparison between theoretical model
and experimental evidences, i.e.\ the morphology observed and catalogued in
morphology databases as Neuromorpho\footnote{http://neuromorpho.org/}.

When we are looking at the neuron from the top of cognitive basic functions,
like spatial or working memory, the morphology appear much less important.
Its relevance is covered by the multitude of neurons we are looking at,
but it is the fundamental process that provide for the establishing of a
neuronal network. Whenever the researchers are interested only to the adjacency
matrix of the network, neglecting the synapses localization and the neurite
extension, the simulation of realistic morphology is necessary. The function of
the graph, and the macroscale properties of the network depend by the number of
connections a neuron successfully establishes. A more concrete example is showed
from three relevant publications on the shelf of culture activity studies: all
the articles use a growth model to establish connections and generate the
adjacency matrix of the culture. In case of~\cite{Orlandi2013} the neuron
morphology was almost absent and the connections between neurons were
established with a very abstract method, this is detailed in
Fig.~\ref{fig:simulator_activity}, the
generated network was used to simulate
the culture and the Calcium Imaging measures compared with those
\textit{in silico}. The second example is more accurate: starting from
NetMorph\footnote{described in next section}
simulator, the authors have studied the variation of synapses density, during the outgrowth of environment
independent neurons. The effectiveness of this simulations cannot be proven directly, but the idea
is more empirical: since it produces plausible independent morphologies the
juxtaposition of many of them will produce something acceptable.\\
Eventually the project in~\cite{gritsun2012} starts from the NetMorph simulator
to build a more consistent network: here the growth cones are driven by
chemotactic gradients and the synapses are computed over density estimation of neurites' fields. Some details are
discussed in Fig.~\ref{fig:simulator_activity}.

\begin{figure}[p]
    \centering
    \includegraphics[width=0.5\textwidth]{images/introduction/gritsun_orlandi}
    \caption{\textbf{morphology like
        simulator for \textit{in
    silico} network edification} Creation of artificial networks of
    biological neurons \textit{in
    silico} requires a heuristic method
    for synapses creation. In picture
    \textbf{1} Soriano, Orlandi \&
    co.\cite{Orlandi2013}
    creates a neural connection whenever
    the extruding axon $l_a$ intersects
    the neurite circle, $\phi_d$ is the
    neurite density. This approach is
    simplistic but very fast and allows
    for creation of huge networks (>
    $5000.000$ units).\\
    Picture \textbf{2} present the
    network generated in \cite{gritsun2012}. It is NetMorph
    based and implements the chemotactic
    based drift. The network is very
    huge in this case and the
    neurites' morphology is plausible,
    unfortunately the code was not
    available on the web and generally
    it presents the flaws of NetMorph
    simulator, it is very hard to
hack.}
\label{fig:simulator_activity}
\end{figure}



In the next section some of the
available and published simulators will be described, with a short description
of pros and cons.

\section{State of the art in neuronal growth simulation}
The history of neuronal growth simulator has evolved in the past twenty years,
first examples were related to the publication itself and the implementation
was very basic, usually the model implemented was detailed in some aspects and
neglect some others. After more sophisticated model were proposed the simulator
started to grow in complexity and took into account the branching functions,
the environment and so on.
A complete review of the active simulator could appear as a boring enumeration,
I have preferred to select the relevant aspects of each one and stress the new
possibility the simulator was offering.

One of the first simulator I found in the bibliography is the one devised by
Segev and Ben Jacob
in~\cite{Segev2017}. They provide a
model for the \textbf{self-wiring of
neuronal networks}, based on
chemotaxis. Their model contains migrating growth cones without detailed neuronal morphology.
The simulation is separated in sequential steps:

1. Each unit \textit{cell} releases a chemo-repulsive agent and sends neurites one at a time.

        2. All newly formed walkers
        \textit{growth cones} are
        initially sensitive to the
        chemo-repulsive agent.

3. After reaching a specific length, the walker switches from sensitivity for the chemo-repulsive agent into
        sensitivity for the
        chemo-attractive agent. The
        walker also emits an amount of
        triggering agent.

        4. The specific length of the neurite is determined by the dynamics of the walker’s internal energy which is controlled
by the soma.

5. The triggering agent diffuses in the media. When the triggering agent concentration exceeds a certain
threshold, cells in the neighborhood respond by emitting the
chemo-attractive agent.

6. Subsequently the walkers move to the attractive response.
This scheme is very simple but effective; the interplay between chemoattraction and chemorepulsion may be keystone in
the understanding of network formation. The investigation proposed in the article are indeed very close to the ones of
NetGrowth project, but twenty years of technological improvements make the simulator obsolete and was impossible to
rescue the code. Furthermore the morphology was absolutely absent in this simulator.\\

A completely different approach is pursued from Luczak in~\cite{Luczak2006} : The main objective of this work is to illustrate
that the creation of complex reproducible dendritic trees does not require precise guidance or an intrinsic plan of the
neuron geometry. Rather that external factors can account for the spatial embedding of the major types of dendrites
observed in the cortex. In this model the number of terminal branches, the mean and maximum branch orders, and the fractal dimension and other
parameters of dendrite geometry are all controlled by a few basic environmental factors. The most important factor in
determining the shape of generated neurons is the space available for growth.
The neurite develops by a
\textbf{Diffusion-Limited Aggregate
model}, described in Fig.~\ref{fig:simulators_old} plus some ad
hoc constraints like spatial limitation, pruning and competition.
Whether this model reproduces strikingly plausible dendritic tree the axon is never mentioned and we believe the same
model will not be sufficient to reproduce the axon outgrowing observed in vitro experiments. This model is however very
relevant to stress the importance of spatial confinement and spatial interactions among branches of the same
neurites.\\

\begin{figure}[p]
    \centering
    \includegraphics[width=\textwidth]{images/introduction/simulators}
    \caption{\textbf{Simulator of
        neuronal growth and network's wiring} Simulate the neuronal growth
        offers many possibilities to study both the growth process and
        simulate activity on large scales. The quest for the graph structure
        of neuronal networks is part of the general investigation on
        information processing in the nervous tissue. Simulators focus on
        different aspect of growth process, intrinsic forces or external stimuli are reproduced to compare the
        difference between model and experimental observations. The four simulations reported in the picture describe
        four different framework of synthetic morphologies. The self-wiring models focuses on creating the relevant
        connections among neurons, neglecting the morphology of neurons. Inversely the \textbf{Environment driven
        model} edifices the dendrites following the particles diffused in cellular matrix and studies the originated
        trees. NetMorph is the most similar project to NetGrowth and implements branching and navigation models to build
    independent neurons. \\
        The compartmental model is a method to integrate the diffusion of a chemo-electrical signal throughout the
    neurite, it is a conventional method to integrate differential equation; the number of equations is set by the
    number of compartments, each compartments act as unit point and solve a discrete differential equation, the rate of
    out-coming and incoming material depends on adjacent segments. This method is not implemented in NetGrowth becouse the
arising system is too much  expensive from computational point of view.}
    \label{fig:simulators_old}
\end{figure}
Just a few years after Randal Koene and
colaborators coded \textbf{NetMorph}~\cite{Koene2009}, which is, up to now, the state of the art simulator. It is based on
fully generative models, developed by the group of Van Oyen and Van Pelt in Amsterdam. NetMorph cannot deal with
spatial confinements and produces only 3D morphologies, the 2D simulator exists but does not work very well.
The merit of NetGrowth was to produce neuronal morphologies based only on internal processes, which are usually derived
by a mathematical modelling of experimental observation. This peculiarity is also a weakness, indeed the importance of
environment interaction was steadily demonstrated with lab's evidences. This time the code was available but was
impossible to fit it to our scopes, the code was intrinsically not modular and we have not find any effective solution
to upgrade it to our solutions. Furthermore it was completely coded in C++, with a not welcoming interface.
NetMorph will be an important touchstone for our project and we hope to be able to reproduce its results and even go
beyond.
Eventually some years ago Torben Nielsen and collaborators proposed a new framework~\cite{Torben-Nielsen2014}, while
the simulator was never finished, the idea of using a full parallelized paradigm for neuronal growth stays. Previously
presented simulators were able to
produce just a few of neurons with
reasonable resources, instead with
\textbf{NeuroMac} the
growth of an entire culture was
possible on personal
computers(easymotion-prefix).
A sketch of NeuroMac an NetMorph is
offered in Fig.~\ref{fig:simulators_old}.\\
To conclude this paragraph let's stress some overall properties of the presented simulations:
\begin{itemize}
    \item All these simulators are generative these means that produce the morphologies from scratch, without any
        statistical mix of observes-reconstructed morphologies
    \item None of the simulator proposes to study the interaction between neuron and mechanical cues, indeed the
        Mechanobiology is a very recent research path. It is linked with the technological improvements of
        last years and is a relatively unknown field.
    \item Only NetMorph is coded with a user interface in mind, others are mostly linked to a specific research
        project. Conversely it is coded with a rigid infrastructure which does not allow for an upgrade towards
        growth-activity simulation.
\end{itemize}
The overall flaws of each simulator is the impossibility to reproduce the exact dynamics of the living matter.
A simplification of the process is necessary to enlarge the scale of the simulation, this problem is an open issue of
computational approaches and the possibility of a deeply multi-scales simulation is just a perspective for the current
state of Bioinformatics. We hope the IT structure of NetGrowth, which is thoroughly modular, will strike a blow in this
direction.

\section{NEST: the activity simulator}
To conclude this brief review on state of the art I have to cite the most widely used simulator for \textit{in silico}
studies of network dynamics.
NEST is a simulator for spiking neural network models that focuses on the dynamics, size and structure of neural
systems rather than on the exact morphology of individual neurons. The development of NEST is coordinated by the NEST
Initiative. \\NEST is ideal for networks of spiking neurons of any size, it can help to test models of information
processing and network activity dynamics, e.g. laminar cortical networks or balanced random networks. One of the
research edge investigation using NEST is in the field of learning and plasticity of networks.\\
NEST is thoroughly parallel and Tanguy Fardet has devoloped a functional module, NNGT to furnish it graph structure and
other topological information, we hope to bridge our research with NEST and use it to develop a framework of
growth-activity simulation.

\section{NetGrowth, a brand new neuronal simulator}
In this section I will present the NetGrowth simulator, I will describe
the collaborations from which it is promoted and point out the new elements it introduces in the panorama of neuronal growth simulators, an example is presented in Fig.~\ref{fig:netgrwoth_sample} \\
\begin{figure}[p]
    \centering
    \includegraphics[width = \textwidth]{introduction/circular_sample}
    \caption{\textbf{A sample culture generated with NetGrowth} NetGrowth simulator can simulate biologically relevant neurons in confinement. The example presents the neurons generated in a Petri dish like culture \textbf{A}. Neurons interact with the environment and create complex shapes, merging the forces imposed by the mechanical traction with the internal attitude to grow out from the soma \textbf{B}. It is possible to evaluate whether two neurons intersect or not and hence create the adjacency matrix for the simulated network \textbf{C}. When the neurons are not constrained they assume the habitual morphology, e.g.\ the neuron in \textbf{D} presents a Chandelier like morphology.}
    \label{fig:netgrwoth_sample}
\end{figure}
NetGrowth is a parallel simulator of cultured (2D) neurons, it simulates the growth process, from the first phase of
neurite differentiation up to mature phase, a period of about three weeks. The neurons can interact with the environment
through their growth cones, the neurite extruding tips. The user can set models and parameters of the simulated cells;
the parameters can be changed during the growth process itself, and, these features permit to reproduce empirical
observation that are not comprised in any model. All the models are correlated with biological evidences and
biophysical models.  The environment
interaction objects to reproduce the \textit{fasciculation} between neurites and the formation of synapses, based on the dendritic
spines evidence. The informatics infrastructure of NetGrowth reproduces closely the one from NEST, the aim of the
project is, indeed, to complement the NEST simulator, towards an integrated simulation of activity and growth.\\

The NetGrowth project is the result of the collaboration among the Neurophysics group at MSC Lab, the group of Catherine Villard
at IPGG, Paris, the group of J.Soriano at UVB, Barcelona and E.Moses, Weizmann
Institute, Rehovot. The research groups are involved in different stages of neuronal
cultures studies, from the axon guidance up to the implementation of learning
protocols for the neurons' population. I had the chance to discuss about
NetGrowth project with the group of Villard and Soriano. They represent two of
the future, possible, users of the simulator: the former is looking for \textit{in silico}
studies to compare the confinement strategies for neurons. The latter needs
realistic network topology and morphology to base the NEST simulations.
I had the opportunity to visit Catherine Lab and I plan to visit Soriano Lab in
May, and, for the occasion, help them to realize the simulation of a new
culture PDMS structure.
The interaction with these groups is a splendid example of coordinated
research: different experiences and competences are joined together at distinct
level of the investigation chain, in order to obtain more reproducible and
consistent results.
In this frame we fulfil the role of theoretical physics and bio informatics,
translating experimental evidences into feasible models, that can be used for
larger scale investigation.
On the other hand the group of C. Villard, in the person of Ian Audric, has
furnished relevant and irreplaceable data whom the simulator is based on.\\

With the NetGrowth simulator, and the NNGT module developed by Tanguy Fardet,
the scientific community of neuroscience is enriched of a sophisticated and
performant tool. Starting from NetMorph, which is the state of the art in 2D
and 3D growth simulation, our project adds consistent improvement.
From the scientific point of view the insertion of mechanical interactions
rules, the possibility of confinement and, in soon, of fasciculation and
chemiotaxic guidance is a significant step forward. This improvement responds
to the technologically advancement in the realization of micro-fluidic
chambers. On the hand of technical features it presents a change of paradigm:
NetGrowth has a parallel infrastructure that responds adequately to IT resources and
trends of today and tomorrow.
Since T.Fardet, who I collaborated with in the developing of the project, has a role
in the Nest developers and users community, the similarity to the \textit{de
facto} simulator of neuronal activity are numerous. We hope to integrate the
projects in the next future, opening the door to very new perspectives: an
efficient simulator of activity and growth, where the electrical stimulation
influence directly the outgrowing properties and vice versa.
Eventually the simulator is realized in favour of a broader community of users
in respect to its predecessors: while the efficiency of the compilation is
granted from the C++ language, with which the models where implemented. The simplicity of use is offered by the user
interface written in Python, the most used language in research labs.

%NetMorph the simulator are governed

%oscale neuronal populations (million or thousands of neurons), each one connected with the others; moving
%inside a population, the role of microscale population appears, the populations
%distinguish into excitatory or inhibitory. These descriptions do not look at the
%morphology of neurons, the adjacency matrix is the fundamental data and they
%mainly investigated macro or meso scale dynamics, i.e.\ the working memory, the
%stimulus response, the spatial memory, etc \ldots.
%To provide for the whole brain network is the scope of connectomics, while to
%discover the effective network governing the meso scale populations remain

%e can go deeper and observe the
%When the modern neuroscience came up in fifties,
%The importance of neuronal
%The aspect the neurona

%In this section we

%When looking to neuronal culture simulation the
%We can read the system state whenever during the simulation and access to
%whichever parameter we are interested too, i.e.\ the intracellular potential.

%\end{document}
