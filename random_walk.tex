% !TeX root = random_walk.tex
% !TeX spellcheck = en_UK
\documentclass[main.tex]{subfiles}
\begin{document}
\chapter{Random Walk characterization}
\label{appendix:random_walk}
\subsection{Random Walk, a short hint}
Random Walk approach is applied in many
fields and the repertoire of problems
is pretty huge, we will follow some
works on Random Walk in
biology~\cite{Patlak1953}, trying to characterize results from \textit{in vitro } experiments and from NetGrowth simulator. \\
The study of long chain of molecules, as polymers, is a lucky application of Random Walk approach. It's relevant to study their properties depending on the micro physics of the anchorage. Another relevant approach descends from statistical mechanic and shades the light on the macro scale behaviour: a sub-chain of strongly connected elements, can be considered under a coarse grain operation, as an unit, hence the constraints between different units are weaker and the long range behaviour presents other properties than short range.
An important objective, among others, is to describe the random walk in a statistical field theory formalism, studying the properties of the polymer chain with the renormalization group. In such formalism the Mean Square Displacement, i.e. the absolute distance between then start and the end of the path, is $\omega_N$, and $N$ is the number of segments.
\begin{equation}
	\eta = \lim_{N\to\infty} \frac{\omega^2_N} {N^{\gamma}}
        \label{renorm}
\end{equation}
is fixed finite and the correct exponent $\gamma$ is investigated~\cite{Procacci2008}.
The algorithms we are implementing are short-range models, since the partial correlation function is an exponential decaying one, and can be demonstrated they ultimately belong to diffusive random walkers as defined in~\ref{renorm}.
The literature we are referring to is generally related to polymers.~\cite{Dorea2014}.
\subsection{Characterization of Random walks: tortuosity, contraction and MSD}
To evaluate the morphology produced by the different algorithms we need to search for some statistical properties: Measures offering relevant information of the studied system too, i.e. the distance reached in a certain time by the extruding growth cone.
All the statistical measures are computed over the real distance from the soma, measured in micrometers.
The assumption made throughout this Appendix is the process is not stationary: the dynamics is not governed only by relative distances and the origin time has any special meaning.
This approximation is relaxed since we expect a strong dependence by time for Growth Cone dynamics. \\
On the other side we assume the system will relax towards a diffusive Random Walk and the persistence length obtained for the first interval of the neurite is the longest possible during neurite evolution.
That is, we expect the GC dynamics will be influenced less by the soma position as far it is from it.
\subsubsection{Square distance from the origin}
The first element we are evaluating is the distance reached by the Growth Cone. \\
The neuron extrudes its active units, the GC, and they will tread a certain amount of distance before attach to a target dendrite. Understand this is a diffusive or a ballistic behaviour is a crucial point.
In this paragraph the length of the segments is considered unitarian and homogeneous throughout the process.
Roughly, we can divide the dynamics into two intervals, a super-diffusive (anomalous) and a diffusive one. The former ends when the distance travelled by the growth cone is far larger than the persistence length of the system. In case of real neurons, if presents, the diffusive limit is barely reached, as can be seen in Fig.~\ref{fig:realneuron}. To simulate the growth pattern of real neurons, it's required to know the persistence length of such neurons and reproduce it with the random walker. \\
%Netgrowth allows to variate the time step of the simulation, that is equivalent to reduce the number of discrete segments required to cover a certain distance. The persistence length can be among all the time scale is enough to divide the persistence length for the time step interval, an example is given in Fig.~\ref{fig:persistence_over_length}.
In the 2D RW the absolute distance from the initial point is:
\begin{gather}
	\langle \Delta X^2 \rangle = \langle (x -x_0)^2+(y-y_0)^2 \rangle \label{eq:msdx}
\end{gather}
the ratio over the curvilinear abscissa can be linear($\alpha =1 $), diffusive ($\alpha =2 $), or present anomalous diffusion with higher powers:
\begin{gather}
	\langle \Delta X^2 \rangle \propto t^\alpha
\end{gather}
This simple measure is enough to characterize the persistence length of the system. recalling the work of\cite{Tchen1989}, assuming stationary state, the MSD is:
\begin{gather}
	\langle X^2 \rangle = \sum^n_0 \langle \vec{b_i}\vec{b_j} \rangle = b^2 \sum^n_0 \sum^n_0 C( i-j) \label{eq:tchen1}
\end{gather}
Neglecting the ends the previous equation can be transformed in
\begin{gather}
	\langle X^2 \rangle = 2 b^2 \sum^n_{s=0} \sum^n_{j=s} C(s) +b^2 \sum^n_{j=0} C(0) 	 \label{eq:tchen2} \\
	S = \sum^n_{s=1} C(s) \\
	M = \sum^n_{s=1} s C(s) \\
\end{gather}
where $S$ and $M$ can be interpreted as the volume below the correlation curve and its first moment. \\
Thus, defined $r^2_0 = n b^2$ the diffusive MSD. It's possible the obtain the mean square displacement:
\begin{gather}
	\langle \frac{r^2}{r^2_0} \rangle = 1 + 2(S - \frac{M}{n}) \notag \\
	\text{for large n:} \\ \notag
	\langle \frac{r^2}{r^2_0} \rangle = 1 + 2S \label{eq:r} \\
\end{gather}
We have verified this relation for the
MCRW in
Fig.~\ref{fig:volumememorystudy2}, The
volume $S$ and the moment $M$ are computed numerically.
\\
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{images/Volume_memory_study3}
	\caption{\textbf{Preliminary study for exponential decaying memory algorithm}\\
		\textbf{a}. The MCRW algorithm, described by Eq.~\ref{eq:memory} is executed varying $\alpha$, then the persistence length is measured as described in~\ref{eq:cosine}.\\
		\textbf{b}. The persistence length as a function of $\alpha$ and $\sigma$, the graphic is fitted by $A*\exp(\alpha * \xi)$ but it shows a super-exponential behaviour.
		\textbf{c}. Eq. The convergence of~\ref{eq:r} is verified: red dots correspond to $\frac{R_0^2}{R^2}$, while blue dots are computed through the integral of the correlation curve.\\
		\textbf{d}. The system converges to $\lim_{n\to\infty} R/R0$ faster with lower $\alpha$. The equilibrium state can be reached in a very long time when $\alpha$ approach to one, and in general reveal the presence of a large transient. In the transient the memory constrained CRW presents a non diffusive behaviour.\\ }
\label{fig:volumememorystudy2}
\end{figure}
For an exponentially decaying correlation function states:
$$S= l_p,\quad M= l_p^2$$
The relation between the memory parameter $\alpha$ and the persistence length should be valuated from the partial correlation coefficient $c_k = \langle \vec{b_i} \vec{b_{i+k}} \rangle$, indeed it's possible to demonstrate that for a finite number of partial correlation:
\begin{gather}
	\langle r^2 /r_0^2 \rangle = \frac{\prod_{j=1}^{k} (1+c_j)}{\prod_{j=1}^{k} (1-c_j)} = 1 + 2 l_p\\
\end{gather}
But nevertheless we know the partial correlation is an exponentially decaying is still hard to compute it analytically for the MCRW. \\
Some attempts follow in next paragraph.
In next section we will show the relation between the MSD here presented and the statistical evolution of angle variable $\theta$.
\subsubsection{Mean Square Displacement of angle variable:}
The second relevant measure, linked with previous is the Mean Square Displacement of $\theta$ over the distance $s$.
$$ \Delta \theta^2 = ( \theta_s -\theta_0)^2= \theta^2_s$$ \\
In this case it's easy to derive a consistent relation with the persistence length for a short interval.
Asserted the global correlation is an exponential dumping function: $C(s) \sim \exp(- l / l_p)$.\\
For $ l \ll l_p$
\begin{gather}
	C(s) \sim 1 - \frac{s}{l_p} + \mathcal{O}(l_p^{-2})\\
	C(s) \sim 1 - \frac{\langle \Delta \theta^2 \rangle}{2} \\
	\langle \Delta \theta^2 \rangle = 2 \frac{s}{l_p}
\end{gather}
This measure naturally defines the \textit{persistence length} as the inverse of the slope:
$l_p = m_{\langle \Delta\theta^2 \rangle}^{-1}$ . This approach is supported by the article ~\cite{Martin2013} which uses this approximation to estimate the persistence length of microtubules.\\
To be consistent about persistence length and correlation time let's reformulate it taking into account the variable length of each segment, the previous equation becomes
\begin{gather}
	\langle \Delta \theta^2(n) \rangle = 2 \frac{s}{l_p b} \label{eq:corr}
\end{gather}
where $b$ is the length of the segment.\\
\subsubsection{Contraction}
The contraction is a non standard statistical measure, often encountered in neurons and general biological morphology evaluations. It's defined as the average ratio between the euclidean distance of the growth cone from the soma and the length of the neurite itself; $\eta$ is always less than 1.
It's showed to be an exponential decay for the CRW we are considering and the $\tau$ is obtained. The Contraction curve inform us whether the diffusion limit was reached or not, indeed the ratio will change, in exponential manner, until the neurite length is much longer then the persistence length, then will establish on a fixed value which correspond to Eq.~\ref{eq:r}.\\
\subsection{Analytic approach to MCRW}
Our interest remains to obtain the persistence length from the parameters in Eq.~\ref{eq:memory}.
Let's make some consideration on this function:
\begin{itemize}
	\item It loses of physical sense when the angle get larger than $\pi$,
	\item Taylor expansion of Eq.~\ref{eq:cosine} requires $n$ remains little.
	\item To simplify the computation the limit of $\alpha ^n \rightarrow 0 $ would be useful.
\end{itemize}
To restore limit $\alpha ^n \rightarrow 0 $  a simple consideration can be offered: for large $\alpha$ we don't need to be very close to the origin, since the angular variation will require larger distance to affect the Taylor expansion, while for little $\alpha$ the correlation will reduce to zero in few steps and the limit is valid.
Let's look at the average values of the stochastic process, keeping $\alpha^n$ for exactness.
\begin{equation}
	\theta_{n+1} = \overline{\theta}_{n+1} + \sigma r_{n+1}
	\label{eq:memRW}
\end{equation}
We note that (\ref{eq:r}) can be rewritten:
\begin{equation}
	\theta_{n+1} = \frac{1-\alpha}{1-\alpha^{n+1}}\sum_{k=0}^n \alpha^{n-k} \theta_k + \sigma r_{n+1}
\end{equation}
\subparagraph{Recursive relations}
For $\theta$:
\begin{flalign*}
	\theta_{n+1} &= \frac{1-\alpha}{1-\alpha^{n+1}}\left( \alpha \theta_n + \frac{1-\alpha^n}{1-\alpha} \overline{\theta}_n \right) + \sigma r_{n+1}\\
	&= \frac{1-\alpha}{1-\alpha^{n+1}}\left[ \alpha \theta_n + \frac{1-\alpha^n}{1-\alpha} \left( \theta_n - \sigma r_n \right) \right] + \sigma r_{n+1}\\
	&= \frac{1 + \alpha(1-\alpha) - \alpha^n}{1 - \alpha^{n+1}}\theta_n + \sigma \left( r_{n+1} - \frac{1-\alpha^n}{1-\alpha^{n+1}} r_n \right)\\
	\text{let}\\
	z_n &= \frac{1 + \alpha(1-\alpha) - \alpha^n}{1 - \alpha^{n+1}}\\
	&\sigma \left( r_{n+1} - \frac{1-\alpha^n}{1-\alpha^{n+1}} r_n \right) \sim \sigma  \left( r_{n+1} - r_n \right)
	%&\underset{n\rightarrow \infty}{\longrightarrow}& \left[1 + \alpha(1-\alpha)\right]\theta_n + \sigma \left( r_{n+1} + r_n \right)
\end{flalign*}
For $\theta^2$:
\begin{flalign*}
	\theta^2_{n+1} = z^2\theta^2_n + 2\sigma z \left( r_{n+1} + r_n \right) \theta_n + \sigma^2 \left( r_{n+1} + r_n \right)^2
\end{flalign*}
\subparagraph{Average values}
If we average, recalling $r$ is a Gaussian with mean zero
\begin{flalign*}
	\langle \theta_{n+1} \rangle &= \frac{1 + \alpha(1-\alpha) - \alpha^n}{1 - \alpha^{n+1}}\langle \theta_n \rangle\\
	&\underset{n\rightarrow \infty}{\longrightarrow}& \left[1 + \alpha(1-\alpha)\right]\langle \theta_n \rangle
\end{flalign*}
this is a simple geometric suite that leads to:
\begin{equation}
	\langle \theta_n \rangle = \left[1 + \alpha(1-\alpha)\right]^n \theta_0
\end{equation}
For the squared value:
\begin{flalign*}
	\langle \theta^2_{n+1} \rangle &\sim 1 + z_n^2\langle\theta^2_n \rangle + 2\sigma z_n \langle \left( r_{n+1} - r_n \right) \theta_n \rangle + \sigma^2 \langle\left( r_{n+1} - r_n \right)^2 \rangle\\
	&\sim z_n^2\langle\theta^2_n \rangle + 2\sigma z_n \langle (1 - \beta) \sigma r_n^2 \rangle + \sigma^2 \langle\left( r_{n+1}^2 - 2 r_{n+1} + r_n^2 \right) \rangle\\
	&\sim z_n^2\langle\theta^2_n \rangle + 2\sigma^2 (1 - \beta) z_n + 2 \sigma^2 (1 - \beta)\\
	&\sim z_n ^2\langle\theta^2_n \rangle + 2\sigma^2 (1 - \beta)( z_n+1)\\
	&\sim a_n \langle\theta^2_n \rangle + b_n
\end{flalign*}
This equation is exact and can be rewritten starting from $\theta^2_0$:
\begin{equation}
	\langle\theta^2_{n+1}\rangle = \prod_{i}^{n} a_i \langle\theta^2_0\rangle +
	\sum_{i}^{n} b_i \prod_{k=1}^{i} a_k
\end{equation}
we should linearize the previous expression for little $n$ and relate to the persistence length through~\ref{eq:corr}, since the variable $n$ is discrete the simplest way is to evaluate the difference between the MSD in $n=1$ and $n=0$, and setting the initial angle to zero:
\begin{flalign*}
	\frac{d}{d n}\langle\theta^2_{n+1}\rangle|_{n=1} = \quad &(a_1 (a_0  \langle\theta^2\rangle +b_0) +b_1 ) -(a_0\langle\theta^2\rangle + b_1)\\
	=& \quad b_1+ b_0 a_0 - b_0 = b_1 + b_0(a_0-1) =  b(z_1+1 ) +b(z_0+1)(a_0-1)\\
	=& \quad b(z_1 - z_0 +z_0^3)\\
	\frac{1}{l_p} \sim \quad \quad & \quad 2\sigma^2(1-\beta) \left[ \frac{1+\alpha-(1-\alpha)^2}{1-\alpha^2} +\left(\frac{1-\alpha(1-\alpha)}{1-\alpha}\right)^3\right]
	\label{eq:linearization}
\end{flalign*}
which is wrong. Indeed the $\alpha$ dependent behaves like an exponent and the persistence length goes to zero for high values of $\alpha$, which is heuristically and numerically verified incorrect.\\
The precious computation remains useful to study the long range behaviour of $\delta^2$. It's calculated that for large $n$ there is a arithmetico-geometric suite which can be rewritten as:
\begin{equation}
	\langle\theta^2_{n+1}\rangle - \gamma \quad=\quad \left[1 + \alpha(1-\alpha)\right]^2 \left(\langle\theta^2_n \rangle - \gamma\right)
\end{equation}
with
\begin{equation}
	\gamma \quad=\quad \frac{b}{1-a} \quad=\quad \frac{2\sigma^2 (1 - \beta) \left[2 + \alpha(1-\alpha)\right]}{ \alpha(1-\alpha)\left(2 - \alpha(1-\alpha)\right)}
\end{equation}
hence:
\begin{equation}
	\langle\theta^2_{n+1}\rangle \quad=\quad \left[1 + \alpha(1-\alpha)\right]^{2n} \left( \langle\theta^2\rangle_0 - \gamma\right) + \gamma
\end{equation}
This result will be useful in later section when considering the angle distribution on a longer time scale.\\
Another approach that can be attempted is compute it numerically: \textit{sagemath}~\footnote{www.sagemath.org} a powerful python tool for symbolic computation has been used for it. $\theta^2$ has been computed up to the third term, and then the linearization performed, subtracting the second term. Hoping the function maintains the same rate in the transient.
\begin{flalign*}
	\langle \Delta \theta^2 \rangle = \langle \theta_3^2 \rangle - \langle \theta_2^2 \rangle \\
	= \left(5  a^{6} + 10  a^{5} + 2  \left(3  a^{6} + 9  a^{5} + 13 a^{4} + 12 a^{3} + 7 a^{2} + 3 a + 1\right) \right) \beta^{3}\\
	- 2 a^{3} - 2 \left(2 \, a^{6} + 4 \, a^{5} + a^{4} - 4 \, a^{3} - 3 \, a^{2}\right) \beta^{2} \\
	- 2 \, a^{2} + 2 \, \left(3 \, a^{5} + 5 \, a^{4} - 3 \, a^{3} - 5 \, a^{2} - 3 \, a - 1\right) \beta\\
	\frac{\sigma^{2} }{a^{6} + 4 \, a^{5} + 8 \, a^{4} + 10 \, a^{3} + 8 \, a^{2} + 4 \, a + 1}
\end{flalign*}
Neither in this case the function $\l_p(\sigma, \beta, \alpha)$ presents the right behaviour and further attempts to perform an analytical estimation of the persistence length won't be done.\\
\paragraph{Angle Mean Square Displacement and Persistence Length}
In this section we have tried to measure the persistence length with numerical simulations.
To reconstruct the proper function, the one to convert the requested persistence length into the right adimensional parameters, let's tackle the problem one parameter at time.
First $l_p$ is obtained as function of $\alpha$ in a purely memory random walk. This function is illustrated in Fig.~\ref{fig:volumememorystudy2} to be super exponential, but since we are interested in moderate values of $l_p$ we can accept the error of considering an exponential function.
Then another fit changing $\sigma$ was attempted, and the following is the formula obtained.
\begin{equation}
	l_p = 7 \exp(-5\sigma + 12 \alpha)  \\
	\alpha = (\ln( l_p / 7) + 5\sigma)/12
\end{equation}
Despite the expectations the implementation of such formula didn't work, indeed the fit for $\sigma$ it's to rude and a little change in its value causes a great variation in $l_p$. The valid alternative was to fit over a 3D manifold, but such computation requires sophisticated approaches, especially if the functional form is unknown.
%In the end we have decided to let the user set the effective length of the memory and the correlation, assumed the relevance of the segment distant $l$ has to be $\frac{1}{e}$.For an average segment length $\langle b \rangle$ we obtain:
%\begin{equation}
%\alpha, \beta = \exp(\frac{- b}{l})
%\end{equation}
%Without any hint for the effective persistence length.
To track values usable in the project have we searched the subset of parameters corresponding to a plausible persistence length, a range between 50 $um$ and 300 $um$. The table is resumed in a 3D plot in Fig.~\ref{fig:plausible}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{images/plausible_values}
	\caption{\textbf{Plausible values for $\alpha$, $\beta$ and $\sigma$}\\
		Here we present the set of parameters offering a plausible persistence length as result, in the range $l_p \in (50,300)\mu m$ The grid search was performed in this manner: first the correlated Gaussian parameter $ \beta $ and the angle of view $ \sigma $ were fixed, then the memory parameter $ \alpha $ was varied from $ 0 $, up to obtain the maximum persistence length.
		$<b_i b_j>$ was measured through the cosine of $\Delta \theta$. \\
		(\textbf{A-B}) show the persistence length reached for each simulation the yellow stripes indicate that was impossible to set a proper $ \alpha $ for that set of $ \beta, \sigma $. In (\textbf{A}) the maximum values of correlation length reached during the grid search. In \textbf{B} only a stripe show values about 50 $\mu m$: for some parameters even very short memory, $\alpha \sim 0$, presents a persistence length longer than the required interval.
		(\textbf{B-C}) show the max and min values of $\alpha$ for the given persistence length. The graph show the importance of $\sigma$ in setting the persistence length. The valid values of $\alpha$ are between zero and one, but when the $\sigma$ is to high ($0.6$), any values is enough to keep the persistence length in the required range.}
	\label{fig:plausible}
\end{figure}
\chapter{Correlated Random Walk: Coloured Noise}
The author in~\cite{DesernoHowTG} offer a simple algorithm to generate correlated Gaussian, with a correlation coefficient $f =\exp(-1 / \tau)$:
\begin{gather}
	r_n = f \cdot r_{n-1} + \sqrt{1-f^2} \cdot \xi_n \\
	r_0 = \xi_0 \\
	C (m-n) = \langle r_n r_m \rangle = f^{m-n}
\end{gather}
In the same paper such system is discussed:
Once defined the correlated and uncorrelated random walks as:
\begin{gather}
	R_N := \sum_{n=0}^{N} r_n  \\
	G_N := \sum_{n=0}^{N} \xi_n
\end{gather}
it's easy to show that:
\begin{gather}
	R_N = \sqrt{\frac{1+f}{1-f}}G_N - \frac{f}{1-f}r_N + 1- \frac{\sqrt{1-f^2}}{1-f} \label{corr_rw_terms}\\
	R_N = \sqrt{\coth{\frac{1}{2\tau}}} G_N	\approx \begin{cases}
		G_N : \tau \ll 1 \\
		\sqrt{2 \tau} G_N : \tau \gg 1
	\end{cases}
\end{gather}
While the second and third term in Eq.\ref{corr_rw_terms} will be of constant magnitude with time the first term will reflect the behaviour of a classic Random Walk; hence it can be seen that the correlated Gaussian random walk is “faster” than the uncorrelated random walk, even though in both cases the increments are Gaussian deviates with zero mean and unit variance! \\
More precisely, the mean square displacement of the correlated walk will grow stronger – in the
case $\tau \gg 1$ - by a factor of $2 \tau$:
Correlation gives distance!
This system can be described as Markovian if the space of phase is enlarged to the parameter $r_n$, and this procedure is thoroughly general.



\end{document}
%% !TeX root = heun_method.tex
%% !TeX spellcheck = en_UK
%\documentclass[main.tex]{subfiles}
%\begin{document}
%\label{appendix:heun_method}
%Here a reference to Heun
%http://mathfaculty.fullerton.edu/mathews//n2003/heunsmethod/Heun'sMethodProof.pdd
%\end{document}
