% !TeX root = models_published.tex
% !TeX spellcheck = en_UK
\documentclass[tesi.tex]{subfiles}
\begin{document}
\chapter{Biophysical models for Growth Cone navigation and Branching}
\minitoc
The evidences discussed in the previous chapter will now be presented in a
biophysical fashion. The main difference is the perspective with which this
discussion is addressed: the trade-off between exact description and
mathematical feasibility is pushed and the experimental evidences give the way
to reproducible models.
In this chapter a bunch of already published models for GrowthCone navigation and neurite branching will be commented, these models were the basis for our modelling work. The chapter is divided into three sections, first the steering models will be discussed, then the GC elongation and eventually the branching process will be take on.

\section{Modelling the growth}

A full model of the neurite development and outgrowth, starting from the positioning of neurons in a Petri dish, would require a proper model of neurite initiation and differentiation. However, we can consider the axon distributed with uniform probability around the soma because of the lack of correlations between separated neurons and the isotropy of the space.

Once the first phase is over, the neurites, leaded by the growth cone, will be able to elongate and retract.
The most relevant neurite is out of doubts the axon, whose elongation rate is
way larger then the rest of dendrites, indeed the most of the articles
concentrate on axon outgrowth.
The study of dendrites and axon will be qualitatively the same: any
evidence of a substantial difference between axon and dendrites elongation mechanism is not reported~\cite{Suter2011}\\
In the absence of a definitive, biophysical, and solved model, the scientific literature offers plenty of particular models -
often with a related \textit{in vitro} experiment that tackle the many faces of the problem-.
The following sections will
review these modelsm highlighting pros and cons.
\section{Steering models}
It was very hard to find reviews or articles about the steering mechanism. This basic process seems to be relevant only for computational neuroscientist willing to reproduce the neuronal outgrowth \textit{in silico}.
The work we have kept in highest consideration during the implementation of the simulator is a previous project of
Torben-Nielsen and Memelli~\cite{Memelli2013}. The authors suggested a environment agnostic model for neurite outgrowth:
morphologies are generated by self-referential forces only. \\
In this model, branching probability follows a Galton–Watson process\footnote{A standard branching process introduced by
	Francis Galton's for a statistical investigation in the extinction of family names. }, and the geometry is determined by
a drifted random walker, governed by internal forces. They modelled these forces, namely an inertial force based on membrane stiffness, a soma-oriented tropism, and a force of self-avoidance, as directional biases over a 3D random walk.
\\These authors didn't publish a detailed study of such forces, preferring to look at the morphologies and benchmark over them. In Fig.~\ref{fig:morphologiesmemelli} the algorithm is described accurately.
\begin{figure}
	\centering

	\includegraphics[width=0.5\linewidth]{images/memelli}
	\caption{	\textbf{Memelli and Torben-Nielsen steering algorithm} The algorithm used in~\cite{Memelli2013} is a superposition of directional vectors computed through the parameters of the different self-referential forces.\\
		The first parameter, the inertial force can be computed on local information only, while second and third forces require the position of the soma and the neighbour cones.
		The former can be easily stored in memory but the latter require full space perception at each step of the simulation.}
	\label{fig:morphologiesmemelli}
\end{figure}

\section{Elongation models}
\label{sec:elongation_model}
In this section we consider three articles that most influenced our project. A preview of different models already used to describe the elongation of growth cones. None of the following articles claims to provide a full model of neuronal growth.\\
\paragraph{A mechanical GC}
Let's start from  a completely mechanical model as the one proposed in~\cite{OToole2008}. The axon is considered
as a viscoelastic system: a series of Burgers elements, springs and dash-pot, as presented in Fig.~\ref{fig:dashpots}.
This system was used to model towed growth of the axon, exerted with atomic force microscopy; the assumptions are that
the axon reaction to force exertion enters in a steady state after few minutes and the system saturates its elastic
properties. By these assumptions the elongation is due only to the dash-pot and is completely passive, induced by the
pulling force; and it is possible to compute the elongation rate. \\
Assuming:
\begin{enumerate}
	\item In the dashpot the resulting speed depends linearly by the exerted force, $v = F/G$
	\item The neurite contributes to the elongation with all the segments and not only by the GC tip
	\item Some segments of the neurite are anchored to the substrate with a dash-pot of constant $\eta$
\end{enumerate}<++>
then a simple system of differential equations can be solved and the following equations state:
\begin{gather}
	%\nu \left[ x, L(t)\right] = \frac{F_0 \sinh(x\sqrt{\eta/G})}{\sqrt{\eta G}\cosh(L(t)\sqrt{\eta G})}\\
	L(t) = \sqrt{(G/\eta)} \sinh(\beta \exp(\frac{F_0}{G} t))
\end{gather}

\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{images/dashpots}
	\caption{\textbf{Model of a towed neurite as a series of dashpots.} \textbf{A} Consider the axon as a series
		of Burgers elements. Each element consists of two elastic elements and a free dashpot (with constant G), which
		simulates towed growth. \textbf{B} Diagram of a neurite during towing. The distal region of the neurite is free of
		the substrate, whereas numerous adhesions in the proximal region cause the neurite to remain firmly fixed.
		\textbf{C} Under constant tension ($F_0$), the behavior of a Burgers element is dominated by its free dash-pot.\\
		The model treats a neurite under constant tension as a series of dashpots. Attachments to the substrate are represented as friction
		dashpots (constant h). Tension is constant in the distal region but is dissipated by adhesions in the proximal region.~\cite{OToole2008}}
	\label{fig:dashpots}
\end{figure}

The model forecasts a stable rate of elongation for each element of the neurite, which ends up in a non linear growth rate at the neurite tip.
This model is inconsistent with the growth cones we want to model, which are not pulled but instead grow spontaneously. However the model is quite simple and solvable analytically.\\

\paragraph{MAP2 diffusion model}
A completely different model is proposed in~\cite{HELY2001}. Here the author consider a compartmental neurite and the
diffusion of a certain protein on it. Such protein is the \textit{Map2} which has a relevant impact on microtubules
fasciculation and stability. The protein can phosphorylate or dephosphorylate~\footnote{In biology, phosphorylation and
	its counterpart, dephosphorylation, are critical for many cellular processes. Phosphorylation is especially important
	for protein function as this modification activates (or deactivates) almost half of the enzymes, thereby regulating
	their function}. The activation (phosphorylation) of MAP2 enzymes reduce the probability of branching and enhance the
elongation rate.
In the hypothesis
\begin{enumerate}
	\item the phosphorylation is regulated by the amount of Calcium in the compartment.
	\item the MAP2 protein is produced only in the soma.
	\item the Calcium influx depens only by neurite diameter.
\end{enumerate}
the author sketches a schema of 4 linear differential equations, reported in Fig.~\ref{fig:map2schema}.
Once the MAP2 dynamics is integrated in a compartmental neuron, the elongation rate $E$ and branching probability $P$ can be computed by the following equations:
\begin{gather}
	E = E^0 k_E MAP2_b \frac{MAP2_b}{MAP2_p} \\
	P_{br} = P^0_{br} k_B MAP2_p \frac{MAP2_p}{MAP2_b}
\end{gather}
This model is very interesting for the simulator project since it is quite omni-comprehensive and accounts for the
elongation rate as well for the branching probability. Nevertheless the solution of the linear system requires a
compartmentalization of the neuron and the compartments number grows exponentially with the branches in the neurite. We
have maintained the general idea of elongation as a byproduct of local factors, the Calcium influx, and soma generated proteins, using respectively neurite diameter and competition as proxies. The link between elongation and branching is
also maintained in the model implemented in NetGrowth.

\begin{figure}
	\centering
	\textbf{Map2 dynamics}
	\includegraphics[width=0.9\linewidth]{images/map2_schema}
	\caption{The cell dynamics simulated in the branching model. Diffusible $MAP2_u$ is produced in the soma. First it is converted into the microtubule-bound $MAP2_b$ complex and then phosphorylated into $MAP2_p$. Calcium enters across the membrane and diffuses throughout the dendrite. The calcium-dependent functions $F$ and $G$ control the on/off rate of phosphorylation. The shape of these functions is shown for various values of the steepness parameter k used in the simulations.}
	\label{fig:map2schema}
\end{figure}

\paragraph{Growth, collapse and stalling}
Collapse and stalling are introduced in a fully biophysical
fashion in~\cite{Recho2016}. The author describes the
neurite as a viscoelastic system, imposing and solving set of conservation equations - conservation of mass,
momentum and rheological assumptions.
The neurite undergoes the forces exerted by microtubule
invasion of growth cone and F-actin meshwork contractility.
This model, which is based on only biological and
measurable parameters - actin and tubulin meshwork
friction parameters, polymerisation rate, etc\ldots-
predict success the existence of three stages of neurite
outgrowing. These three phases, growth, collapse and
stalling, are regulated by the load at the end of the
neurite, generally a pulling force. Whether the growth cone
is free to elongate the pulling force is the actomyosin
molecular motors that pull on the focal adhesion created by
the growth cone on the substrate.
Let's define some variables: $a=\frac{\zeta_c}{\zeta_\mu}$
is the ratio between the friction of microtubule with the
neurite cortex and the friction of cortex with the
substrate; hence the Growth Cone exerted force is $Q_{gc} =
\sqrt{a} v_p$, where $v_p$ is the polymerisation rate of
G-Actin in F-Actin.
The derived equations are quite complicate but can be
summarized in three regimes:
\begin{itemize}
    \item \textbf{Retraction}, whether $Q_{gc}$ is less then the
    stress exerted by the microtubule turnover $Q_{\mu}$
    \item \textbf{Stalling}, if $Q_{gc}$ is comparable with
    the friction of the neurite cortex $Q_s$ and microtubule
        turnover, and the total length is predicted in this
        case.
    \item \textbf{Elongation} with velocity
                \begin{equation}
                    V = \frac{Q_{gc} - Q_s} {2\sqrt(a) +B}\\
                    \label{eq:recho_velocity}
                \end{equation}
        where B comprehend parameters we have not
        introduced, generally linked with neurite width and
        actin viscosity.
\end{itemize}
This model is very well done, both for the deepness of
computation, where each assumption is justified with
experimental evidence, both for the overall simplicity of
the predicted results.
To conclude its study the author has performed
pharmacological studies, in very good agreement with the
predictions. Fig.~\ref{fig:recho_model} summarizes the
resultss
\begin{figure}[p]
    \centering
    \includegraphics[width=\textwidth]{models/recho_model}
    \caption{\textbf{Growth, collapse and stalling from
    biophysical quantities} The model proposed by Recho,
and summarized by Eq.~\ref{eq:recho_velocity}, starts from
    a simple depiction of the growth cone internal forces,
the one proposed in \textbf{A} and develop a set of
viscoelastic equations. After some evidence-based
assumptions the model is simplified and a two thresholds
are derivate: a stalling threshold and a retraction one.
The first depend by the microtubule turnover rate, while
the latter by the contractile load of actin cortical
network. Picture \textbf{B} show the growth, stalling and
retraction regions varying the pulling load, i.e.\ growth
cone force in our case, and the parameter $\epsilon$, this
is the ratio of acto-myosin molucular motor over the
microtubule network viscosity~\cite{Recho2016}.
\label{fig:recho_model}}
\end{figure}
We have replicated the existence of this three thresholds
in NetGrowth, but there is not an equivalent model for the stresses and
frictions exerted by the neurite components. We hope to
implement this model and verify the reproducibility of
Recho's results in a future model of NetGrowth.


\section{A mathematical picture of neurite branching}
Let's now tackle the complex problems of GC generation. The initiation of branches -and the consequent arborization-
shapes the neurites, their computational properties and the number of synaptic connections. It is fundamental to reproduce the neurite tree adequately.\\
Several modelling approaches have been used in the \textit{in silico} synthesis of dendritic trees. These can be characterized either as Growth Models or Reconstruction Models. Growth Models are based on principles of dendritic development, using rules of outgrowth associated with dynamic growth-cone behaviour and microtubule-mediated neurite elongation. In contrast, Reconstruction Models use an algorithm based on a canonical set of elementary properties which are originally derived from the characterization of existing dendritic structures~\cite{Polavaram2014}. Note that Reconstruction Modeling is a purely descriptive approach which uses minimal rules to \textit{synthesize} topologically-realistic neurons.\\ In contrast, GrowthModeling adopts an exploratory approach by using biological rules of development and observations of the outgrowth process to explain or predict variations in full-grown arbor structures.\\
These two modes of branching give rise to different geometrical patterns of axon branching and generate different aspects of neuronal circuitry. For the NetGrowth simulator the choice was obligatory, a full generative model was due to mimic the wall-GC interactions.

\paragraph{BEST model}
Altough the biological mechanisms and the relevant parameters were not fully understood,
Dutch researchers Van Pelt and Van Oyen gave a great contribution to the understanding of outgrowing neurites.
They have published several articles~\cite{VanOoyen2014,VanPelt2003,VanPelt1997} to face this problem with a simple
and effective mathematical model. In~\cite{VanPelt2003} they propose the \textbf{BEST} model: the branching mechanism is
GC splitting only and the parameters are completely phenomenological. Given the number of segments \textit{n}, the
competition parameter among GCs \textit{E} and a baseline distribution for the branching rate
\textit{D(t)}~\footnote{The branching baseline is necessary to give a temporal frame to the BEST model: the first implementation
	\cite{VanPelt2003}, used discrete distribution and constant time binning, without any relation to time. Such a model
	whether reproduced the right
	statistical measures were wrong in terms
	of branching time.}, the growth cone branches with probability:
\begin{gather}
	p_{i}(t|n(t)) = \mathcal{N} n^{-E} 2^{-\gamma_i S} D(t) \\
	\text{with the normalization factor}\\
	\mathcal{N} = \sum^{n(t)} 2^{\gamma_i S}
\end{gather}
It is possible to show, for an
exponential decaying baseline, the number of branching events is limited:
\begin{equation}
	n_B(t) = n_{\infty}^{1-e^{-t / T}}
	\quad \text{for E>0}
\end{equation}
Here $ \gamma_i $ is the centrifugal order\footnote{Centrifugal order is the number of nodes separating the node $ i $
	from the root} and the parameter $ S $ is a symmetry parameter
a sketch of such process is offered in Fig.~\ref{fig:vanpelt}.\\
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/branching/vanpelt}
	\caption{\textbf{VanPelt BEST model} The picture represents the time evolution of two Van Pelt trees. The constant baseline function – namely $ D(t) = B/N$ – is the average number of branch events. The sketch shows two different sequences, where the same stochastic model generates two different trees. The time component $ D(t)$ is introduced to fit real neuron measurements. Switching from a discrete-time to a continuous-time probability function impacts the theory and prevents the number of branching events from divergence thanks to the exponential decay of the function $ D(t) $ \cite{VanPelt1999}}.
	\label{fig:vanpelt}
\end{figure}
BEST model is an artificial solution, where parameters have to be fitted with experiments and can variate slightly from
a neuron type to another. The converging integral $ \int_0^{infty} dt D(t) = AT$ can be associated to the average number of
branching events $ B $, while the time scale is fixed by the characteristic time $ T $. The parameters optimization
is performed considering the mean and the variance of measured real neurons, as those retrievable in the NetMorpho database.
\subsection{Many GCs compete with each other}
\label{chap:competition}
The pruning introduces the problem of competition for the growing neurites. Which branch is going to collapse? Is the optimization process of the tree structure local or global? Is possible to model arborization as a competition between growth cones?\\
The microbiology of neurotransmitters that trigger this process is a complex subject, but we can give a general idea of it by a simple mathematical model. In fact, the competitive dynamics introduced by \cite{Hjorth2014} offers a simple perspective over the whole phenomenon. Some elements – required by the GC to synthesize microtubules and then elongate – are built into the soma, thereby needing to be transported to the very end of neurite's tree in order to be available for the growing GC. This transportation can be diffusive or active – i.e. done by molecular motors – and the two cases lead to very different results. This simple idea was implemented with a compartmental simulator, where each segment of the neurite stores or consumes some of the limited material. In the cited work, the referred protein is tubulin. Fig. \ref{fig:tubulin} presents a scheme of its production and polymerization. During retraction tubulin is depolymerizated.\\
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/branching/tubulin}
	\caption{\textbf{Tubulin dynamics in the model}. Tubulin molecules (green spheres) are produced in the soma by biological neurons via translation of mRNA on ribosomes (the brown structure). Tubulin is then transported by diffusion and active transport. In biological neurons, the microtubule bundles (the light green fibers) act as railway tracks on which the tubulin molecules are bound via motor proteins (the red molecules). Tubulin is transported to the growth cones at the tip of the neurites. At the growth cone, tubulin is either integrated or polymerized into the microtubule cytoskeleton (long polymers of tubulin dimers – green fibers), which elongates the neurite. When the microtubule depolymerizes, the neurite retracts and tubulin becomes free again.}
	\label{fig:tubulin}
\end{figure}
The equations of the model can be condensed in the following one that represents the evolution of the amount of tubulin $ Q_i $ in a certain compartment:
\begin{equation}
	\frac{dQ}{dt} =D f(Q,A,V) + \nu g(Q,A,V) -bQ -X\frac{dL}{dt}
\end{equation}
the equations govern the evolution of the tubulin amount $ Q_i $in a certain compartment, the r.h.s first term is the
diffusive one and is moderated by the diffusion coefficient \textit{D}, the second term represents the active transport
regulated by $ \nu $, \textit{b} is the tubulin corruption rate and, eventually, the last term $ X \dfrac{dL}{dt} $
accounts for the consumption of tubulin in the growth cones. The tubulin diffusion ODE is associated by an elongation
ODE for each terminal segment:
\begin{equation}\label{key}
	\dfrac{dL}{dt} = p Q -q
\end{equation}
The elongation rate is defined as the result of the polymerisation rate \textit{p} times the tubulin \textit{Q} in the
leave, minus the depolymerisation rate \textit{q}. An experiment with this model is detailed in
Fig.~\ref{fig:tubulincompetition}. \\
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/branching/tubulin_competition}
	\caption{\textbf{Competition between neurite branches in a complex morphology}. (A) Example morphology of a reconstructed pyramidal neuron with apical and basal dendrites. (B) In the control case, starting from the reconstructed morphology, the neuron was allowed to grow out for 10 hours in the model. The simulation was then repeated with the same initial conditions, but with an increased polymerization rate for one of the growth cones. The dendritic morphology obtained in this last simulation is represented by a dendrogram, coloured according to the tubulin concentration in the branches. The gray vertical lines at the terminal segments indicates the starting morphology, and the black vertical lines show the neurite's length after 10 hours – in the control case. The black dot marks the growth cone with increased polymerization rate. (C) The competition between branches increases with increasing path distance to the soma. The graph shows the total retraction of all neurites divided by the growth of the modified growth cone, as a function of the path length between the modified growth cone and the soma. \cite{Hjorth2014}}
	\label{fig:tubulincompetition}
\end{figure}
The model is quite simple but requires
the integration of the whole ODE step
by step and is not very suitable for an efficient simulator.
The competition model for critical resource we will introduce later is largely inspired by this model.
\subsection{Optimal wiring and scaling laws}
Competition among growth cones can be by another
perspective, like the one proposed by Cuntz and
collaborators\cite{Cuntz2010,Cuntz2012}. Instead of
generate the neurite step by step through growth cone
migration, the authors of TREESToolbox~\footnote{\url{http://treestoolbox.org}{TREESToolbox}} have proposed an
optimization-driven approach for neurite reconstruction:
the algorithm builds tree structures which minimize the
total amount of wiring and the path from the root to all
points on the tree. On the basis of Ramon y Cajal cytoplasm conservation law, they propose three more detailed principles:
\begin{itemize}
    \item By adjusting the balance between the two wiring costs -total dendrite length and distance from the soma-, a dendrite can efficiently set its electrotonic compartmentalization, a quantity attributable to computation.
    \item The density profile of the spanning field in which a dendrite grows determines its shape dramatically.
    \item A few weaker constraints such as the suppression of multifurcations, the addition of spatial jitter or the sequential growth of
        sub-regions of a dendrite are helpful for reproducing the dendritic branching patterns of particular preparations.
\end{itemize}
Unfortunately there is not any direct application of these additional constraints in a generative simulator. Nonetheless they might shed light on
further functional, computational, developmental or network determinants for certain dendritic structures, we have tried to take in to
account these constraints in the competitive model, e.g.\ setting an explicit attenuation of the critical resource received when the -user
defined- total dendritic length is exceed.
Most of all the simple principles presented in this study can be used to efficiently edit, visualize, and analyze neuronal trees and will
be used to validate the morphology generated. \\
On this purpose the result accomplished in \cite{Cuntz2012} is even more relevant: although the wide diversity of dendritic trees, they
have developed a general quantitative theory relating the total length of dendritic wiring to the number of branch points and synapses. The
optimal wiring presented above predicts a 2/3 power law between these measures and they show the theory is consistent with data from a wide
variety of neurons, across many different species.

\section{Gateway to stochastic models}
The discussed models are deterministic as a consequence of solving the macroscopic equations of mechanical or chemical
processes. However the simulation of such complex events cannot be fully deterministic since the phenomenon undergoes
non-equilibrium statistical mechanics. A weak argumentation is to consider the many undergoing processes we are neglecting, proteins
diffusion, synthesis, and the chain effect on the growth itself; the branching itself is depicted by a system with a
variable number of ODEs (equation are doubled after first branch). This is enough to expect a deeply stochastic process
and to introduce stochastic elements into the simulation.\\
The Netgrowth simulator models the outgrowth dynamics of axons and dendrites with stochastic rules, at many levels. The steering is regulated by a random walk, the branching is mediated by a stochastic model and the neuron position is drawn by a spatially uniform distribution.
\end{document}
