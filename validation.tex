% !TeX root = validation.tex
% !TeX spellcheck = en_UK
\documentclass[main.tex]{subfiles}
\begin{document}
\chapter{From neurons to networks, validation and graph creation}
\minitoc
\section{Validating Netgrowth morphologies}
The validation of NetGrowth simulator is a fundamental part of the
project: the morphologies and networks generated should be a close reproduction of those observed in
neurobiology laboratories. Some test cases on which NetGrowth could be used are the following:
\begin{itemize}
	\item
            \textbf{How does the inter population connectivity change when the average branching rate is increased?}
	      To aid neurobiologist in relating the macro-scale properties of single neurons and neuronal networks on Petri dish, i.e.
	      connectivity, graph closeness measures, arbor extension, etc\ldots, to the micro-scale processes and
	      parameters: The modularity of the simulator allow for the elaboration and implementation of new models
	      in a parallelized simulator, and we hope once the simulator will be ready to use some labs will pay
	      attention to its possibilities.
          \item \textbf{What is the resulting network of a certain culture structure? How do topologies affect synaptic
                      plasticity?}
	      To furnish plausible networks and morphologies to theoretical researchers fuels the investigation of
	      more plausible network topologies, which, integrated into activity simulator, e.g. NEST, allow for the
	      simulation of cultured neurons dynamics. The verisimilitude is a necessary requirements to keep the
	      investigations relevant and prone to applications.
\end{itemize}
Having stressed the importance of obtaining realistic neuron morphologies and effective environment interactions,
then comes the hardest and trickiest part of the whole project: testing the verisimilitude of the generated neurons
and networks. As generally done in science the \textit{dividi et impera} approach should make the case, indeed the
properties to test are many and verifying the branching rate together with the elongation model could not unveil the
insufficiencies of one or the other formal descriptions. The simulator should be confirmed by appositely conceived
experiments, as those used to benchmark the environment interactions. These experiments require time and competence, and
were not performed during the short time of my master thesis.\\

On the other hand one of severe lessons I learnt during these project is what happens inside a cell is never obvious:
The chemo-electrical processes going on beyond cellular membrane are tightly connected, and to study them separately is not an easy achievement.
It is possible, for this purpose, to inhibit some mechanisms, as the branching, with drugs or genetic manipulation, but it requires an
entire research project and often offers only qualitative results.
Furthermore the behaviour of the single neuron depends thoroughly by the environment, i.e.\ the rest of the culture.
Neurons are not solitary, either their lifespan is strongly
correlated to their environment and to the presence of other cells.
Glia, for instance, are essential to feed the neurons and form synaptic connections, same how the neuron will not
survive if they don't establish synaptic connections, and when they do they prune large part of their body which is not
functional to its scope: receive and transmit electrochemical signals.
%During my short presence in IPGG and SISSA I discovered the complexity and uncertainty behind biology experiments: often
%the protocol does not work for unpredictable reasons, e.g.\ talking with Ian Audric told me, about the experiment with the
%diode, the most of prepared neurons died and the numbers of those survived and actually interacted with the diode
%structure was too little for a consistent statistics.\\
With these premises to test the NetGrowth simulator would require a long session of experiments, the parameters should be
fine tuned with observation and accurately related to live cells behaviour; different environment should be testes and see whether they affect or not the growth and how. This was not possible in the context of my thesis, therefore only a short hint of validation will be presented in this manuscript.\\

The validation will be divided in two sections, in the first I will review two articles which offer a consistent blueprint of measures, towards an integrated and effective statistic of neuron morphology. In the second I will produce these measures for some morphologies generated with NetGrowth.

\subsection{Measuring neuron's morphologies}
The articles we are referring for morphologies validation are~\cite{Costa2010,Snider2010}. The authors of these review
concentrated on general properties of neurons' morphology, stressing the relevant aspects of neurites, Fig~\ref{fig:neurite_variables} and the consequent measures can be performed to classify these neurons and to validate
\textit{in silico} generated neurons. These approach unveils the most of neurons in a reduced area of the 2D space,
which is very reduced in respect to the neuron that can be generated \textit{in silico} both with generative and
statistical procedures.
Anyway a relevant properties of neuronal morphology is the spatial density function, a function that specifies the
probability of finding a branch of a particular arbor at each point of the real space. From the analysis of over a
thousand arbors from many neuron types in various species, \cite{Snider2010} has discovered an unexpected simplicity in
arbor structure: all of the arbors examined, both axonal and dendritic, can be described by a Gaussian density function
truncated at about two standard deviations. These result, very surprising indeed, is valid for neuron growth with a
completely isotropic space and can be very useful to verify the quality of morphologies produced with NetGrowth. In
respect to the previous analysis the classification is reduced to four parameters in 3D and three in 2D: the total
length of dendritic tree and the 3(2) parameters for the standard deviation in each dimension, an example is reported
in Fig.~\ref{fig:neurite_density}.
Another standard measure can is the Sholl Analysis: a method of quantitative analysis commonly
used in neuronal studies to characterize the morphological characteristics of an imaged neuron \cite{Sholl1953}.
It was first used to describe the differences in the visual and motor cortices of cats. While methods for estimating number of
cells have vastly improved since 1953, the method Sholl used to record the number of intersections at various distances
from the cell body is still in use and is actually named after Sholl. In order to study the way in which the number of
branches varies with the distance from soma, it is convenient to use a series of concentric spherical shells as
co-ordinates of reference these shells have their common centre in the soma. Sholl also realized his method is useful to determine where
and how big is the region where synapses are possible, an example of Sholl analysis on a real neuron is offered in
Fig.~\ref{fig:neurite_density}


\begin{figure}[p]
    \centering
    \includegraphics[width=\textwidth]{validation/neurites_variables}
    \caption{\textbf{The variables of neuromorphology space}The neurites are
        complex structures and their geometry and topology can be classified on
        the base of 20 parameters, picture \textbf{A}. The variables can be
        global, as the number of total branches, or local, (i.e.\ the distance
            between two branching points, the angle between two sibling
            branches, etc\ldots. The author proposes a PCA
                description, which maps the many variables in a 2D space,
                picture \textbf{B} which is more easy to visualize and compare,
                in the picture the grey spots are the morphologies produced in
                \textit{in silico} while the black spots are those obtained by
                the NeuroMorpho database.~\cite{Costa2010}}
    \label{fig:neurite_variables}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[width=\textwidth]{validation/density}
    \caption{\textbf{Density of dendritic arbor and Sholl analysis} A broad
        study, on thousands of reconstructed morphology, shows the neurite has
        a density function which is Gaussian isotropic space, picture
        \textbf{A}, and proposes that four parameters are enough to describe
        the dendritic arbor, the dendritic arbor length and the standard
        deviation in each direction.\cite{Snider2010} A similar analysis,
        picture \textbf{B}, is used since 1953 to classify neuron morphologies,
        its inventor, which gave the name to the method, asserted Sholl
    analysis is valid to estimate the probability of creating a synaptic bottom
at a certain distance from the soma.\cite{Sholl1953}}
    \label{fig:neurite_density}
\end{figure}

In the next section the morphologies produce with NetGrowth in isotropic space will be validated with the method of
arbor density.
\subsection{Simulated and cultured neurons}
This section is indeed very relevant for the scientific validity of the work I have done for the NetGrowth simulator.
Since the simulator will produce morphology for confined culture a sound validation of the morphologies should be done
in that direction. To studies these morphologies \textit{in vitro} it is, sadly, very hard. The arising complications
are many, one over all the death of the selected neuron. Furthermore the acquisition of the image into a descriptive
format is not trivial, many software exist to do it and the most are proprietary and sold with the microscope itself.
Eventually we have to consider the evaluation of the morphology cannot be done on general basis, indeed the
morphology depend deeply by the parameters of the experiment: the area of the brain whose cells belong, the species of
the animal, the conditions of growth and the time of the neuron. It is evident that a sound work of validation should
be done with the help of a neurobiologist and with the facility of a neurobiology laboratory. These accomplishments are
well beyond the possibilities, in terms of time and knowledge, of this manuscript and are left open. Anyway the
measures themself will be performed on the produced neurons and attached, both to prove the results are similar to
those we can find on publications and in the web in general, and to figure out how the conclusion of the NetGrowth project
will appear.\\
Even to find neurons in 2D in the web is a hard task, indeed the NeuroMorpho database collects only neurons
obtained from \text{in vivo}, and it lacks of neurons growth on Petri dishes. Since the following analysis is
completely arbitrary I opted to pay tribute to Santiago Ramón y Cajal, the
first neuroanatomist. He was fascinated by the
complexity of neurite arbor and draw a huge quantity of hand-made reproductions of neurons from \textit{in vivo}, his
work was the basis for dendritic arbor studies for half of a century. Hence I
based on his picture in Fig.~\ref{fig:ramon_y_cajal} to draw, \textit{by
keyboard} some plausible morphologies. The morphologies I
chase are the Chandelier interneuron
in Fig.~\ref{fig:Chandelier} and Multipolar cell Fig~\ref{fig:multipolar}. I
tuned the parameter by hand, basing on the experience, and the default
parameters obtained from articles and review, to reproduce these
morphologies. The description of the parameters is reported in the picture.


\begin{figure}[p]
    \centering
    \includegraphics[width=\textwidth]{validation/cajal}
    \caption{\textbf{Different Types of Neurons drawn by S. Ramón y Cajal} \textbf{A}. Purkinje cell \textbf{B}. Granule cell
        \textbf{C}. Motor neuron \textbf{D}. Tripolar neuron \textbf{E}. Pyramidal Cell \textbf{F}. Chandelier cell
    \textbf{G}. Spindle neuron \textbf{H}. Stellate cell, based on
reconstructions and drawings by Cajal.
Ramon y Cajal was a pioneer investigator of neurite morphologies and
function, he has gained from is observation some fundamental rules that are
still in force, e.g.\ the law of cytoplasm conservation.}
    \label{fig:ramon_y_cajal}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[width=0.5\textwidth]{validation/Chandelier}
    \includegraphics[width=\textwidth]{validation/interneuron_shokk}
    \caption{\textbf{Chandelier neurons} or chandelier cells are a subset of
        GABA-ergic cortical interneurons. Chandelier neurons synapse
        exclusively to the axon initial segment of pyramidal neurons, near
        the site where action potential is generated. It is believed that
        they provide inhibitory input to the pyramidal neurons, but there are
        data showing that in some circumstances the GABA from chandelier
        neurons could be excitatory. \\
        The neuron is generated gathering the
        all the model at different time: initially the axon is left to
        elongate, while the dendrite suddenly undergoes growth cone
        splitting. Hence the dendrites complete the branching phase and start
        to elongate in a symmetric competition phase. The axon instead starts
        to branch, both laterally and growth cone splitting, the persistence
        length is set about $40 \mu m$ but the turning angle is broad $0.5
        rad$, this produce a characteristic \textit{broken line} aspect to the
        neurite. In the four plots are presented the Sholl analysis and
        density analysis for a set of 400 generated neurons. The colors in
        Sholl analysis distinguish different neurons.}
    \label{fig:Chandelier}
\end{figure}
%\begin{figure}[p]
    %\centering
    %\includegraphics[width=\textwidth]{validation/purkinje}
    %\caption{\textbf{Purkinje neurons} These cells are some of the largest neurons in the human brain, with an intricately elaborate dendritic arbor, characterized by a large number of dendritic spines. Purkinje cells are found within the Purkinje layer in the cerebellum.
%}
    %\label{fig:purkinje}
%\end{figure}
\begin{figure}[p]
    \centering
    \includegraphics[width=0.5\textwidth]{validation/multipolar}
    \includegraphics[width=\textwidth]{validation/multipolar_shokk}
    \caption{\textbf{Multipolar neurons} A multipolar neuron is a type of
        neuron that possesses a single axon and many dendrites (and dendritic
        branches), allowing for the integration of a great deal of
        information from other neurons. These processes are projections from
        the nerve cell body. Multipolar neurons constitute the majority of
        neurons in the central nervous system. They include motor neurons and
        interneurons and are found mostly in the cortex of the brain, the
        spinal cord, and also in the autonomic ganglia.\\
        The neuron is generated regulating the growth cone splitting to start
        at different times. The axon elongates rapidly with a strong
    persistence length, while the neurite branches and compete each other.
Hence the growth cone splitting is turned on and the axon starts to branch.
The other pictures present the Sholl analysis and the density profile for a
set of 400 neurons, while the density of dendrites is tight to the soma, the
axons cover a large ring, obviously the diameter of the ring can be tuned by
parameters.}
    \label{fig:multipolar}
\end{figure}

\section{NetGrowth simulated cultures}
The scope of NetGrowth is not only to produce valid isolated neurons but to create complex networks simulating the growth of neurons in complex confinements. In this section two example of such ability will be offered, the first is a very classic configuration, with hundreds of neurons contained into a Petri dishes. The second example is derived instead from the set of experiments realized by R. Renaud at IPGG, in this case the neurons will be confined in a two-chambers culture. The number of neurons is usually around hundreds of thousands in these cultures, but the simulations were performed on a personal computer, whose limited RAM and CPU does not allow for such a huge simulation. The analysis of the networks obtained is beyond the scope of these thesis: the scenarios which are opened with this simulation are numerous and wide. For instance the degree distribution can be investigated and compared with \textit{in vitro} measured degree distribution. Hence it is possible to compute the centrality of neurons, and see whether their inhibition slows down the activity of the whole culture.\\
The very important step is to connect the neurons each other throughout the synapses; these chemoelectrical buttons are among the most amazing and complicated elements of neuronal dynamics studies: they can be excitatory or inhibitory and the understanding of the most complex functions of nervous system pass by them. The general mechanism is the following: the pre-synaptic neuron releases chemical elements into the synapse, these are absorbed by the post-synaptic and activate or inhibit the ion channels, favouring or preventing the rise of dendritic spikes. Synapses are not considered now, in next sections the neurons will be considered connected whether the intersection among the dendritic tree and the axon's branches is not empty.
\begin{figure}[p]
    \centering
    \includegraphics[width=\textwidth]{cultures/petri_}
    \caption{\textbf{NetGrowth generated neurons in Petri dish culture} The
    figure presents results from a Petri dish like confinement with 100
neurons.The neurons are growth within the environment with BEST model
branching model and run and tumble navigation model. The data produced are
saved in SWC format and post processed with tools I developed within
NetGrowth project. Picture \textbf{A} reproduces the adjacency matrix
obtained from neurite intersection method: whether an axon intersects the
dendrite of another neuron a synapse is generated. Pictures \textbf{A.1} and
\textbf{A.2} describes the density of neurites, both axon and dendrites and
the occurred synapses. The correlation is evident, this is due to the
triviality of intersection method.}
\label{fig:culture_petri}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[width=0.8\textwidth]{cultures/diode_neurites_}
    \caption{\textbf{NetGrowth generated neurons in diode-like culture} The
    culture implemented by R. Renault in \cite{renaud_thesis} is reproduced
with NetGrowth and the relative network is outlined. While the density of
neurite is homogeneous in the two chambers the effect of axons invading from
left to right is showed by the higher density of synapses in the right
chamber: the axons from the left forms several synaptic buttons within the
chamber. This configuration is clear in the last picture: left axons in red invade the other chamber with more intensity of right blue axons, this inhomogeneity is due to the shape of the communication channels. }
    \label{fig:culture_diode_neurite}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[width=0.7\textwidth]{cultures/diode_adjacency}
    \caption{\textbf{NetGrowth generated neurons in diode-like culture,
    adjacency matrix} The adjacency matrix for the diode like culture shows
clearly the global properties of the network: some neurons from the left
chamber (IDs less then 100), have some neurons with synaptic buttons both in left
chamber and in right chamber(IDs more then 100), i.e.\ the neurons between 75 and 100. Of the
neurons in right chamber just a few success to overcome the diode-like
structure. This configuration reproduces a perceptron and is one of basic step towards a neuronal based computing device.}
    \label{fig:culture_diode_adjac}
\end{figure}


\section{Conclusions}
In this chapter I have introduced some general measures that can help in
validating the morphologies produced with NetGrowth. The validation of the
produced neurons is a required step to introduce our simulator in the
scientific community. Indeed, a severe control is necessary before use
NetGrowth as a trustable tool, it will not be possible to commend the
simulator to produce networks if we are not sure it can produces realistic neurons.\\
Nonetheless the importance of the validation phase I was not able to fulfil a
sound and rigorous characterization in the framework of my thesis. To mend
this fault I generated and discussed some neurons, following the pictures of Ramon y Cajal.
The model and the parameters
used are described in the Fig.s together with a sketched Sholl and density
analysis.
In the second section I have generated two examples of cultures, one as a Petri dish like culture with neurons
contained in a circular confinement, the other as the standard diode, borrowed by~\cite{renaud_thesis}. For both the
culture I sketched a naïve analysis of the network properties, i.e. the adjacency matrix, and graphically described the
position of neurite and synapses. After the validation of morphologies will ensure NetGrowth is a effective generator
of neuronal morphologies this second phase will be improved with more efficient synapses' creation algorithms.

\end{document}
