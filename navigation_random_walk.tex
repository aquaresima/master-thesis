% !TeX root = navigation_random_walk.tex
% !TeX spellcheck = en_UK
\documentclass[tesi.tex]{subfiles}
\begin{document}
\section{Growth Cone migration: Steering as a random walk in 2D}
The easiest way an agent can explore the space is carrying out a 2D Random
Walker, a Markovian process largely used in physics. We want to verify if the
RW reproduces adequately the navigation of the Growth Cone on the substrate.
The hypothesis is the biological procedures to steer and explore the
environment can be mimicked with a proper algorithm. Hence the algorithm is expected to simulate the migration of a Growth Cone in a physical constrained environment.\\
In the first section I will introduce the problem and offer a general picture of the GC dynamics in the NetGrowth simulator. In the second section the relevant properties required to the algorithm will be presented, some algorithms will be outlined and the results discussed.
Eventually some real neurons will be analysed and compared to the simulator.
This short section focused a relevant part of the project: the navigation is a fundamental aspect of the whole simulation. Some tools, theoretical and computational were used to characterize the algorithms, they are discussed in Appendix\ref{appendix:random_walk}
\subsection{Modelling the elongation with a random walk}
The random walk is very easy to simulate and code, to associate it to a Growth
Cone is straight-forward: the active walker is the GC while the previous
location are joined up to form the neurite. This simple fact implies the walker cannot cross itself constantly and the local aspect of the trajectory is relevant. Indeed in physics or finance the random process are often 1D and in general the history is not relevant.
A larger category of walker are the self-avoiding paths, but they have not been used in the present work for they are computationally expensive.
Some classical algorithms, whose analytic feasibility make interesting, like the Ornstein-Uhlenbeck process, were set aside for they are definitely not self avoiding and the trajectory cannot resemble anything biologically consistent\\
The Growth Cone moves on the \textit{xy} plane but it's evident the random walk is actually on the angle. For a subset of algorithms the step can be fixed and the whole process depends by the distribution of new angles. In this case a simple consideration is that the GC goes as much straight as the angle changes slowly. Such a naive observation hides the complexity of the relation between the mean square displacement in 2D and the one regarding the angle variable.\\
Hence the coordinates system used is the polar system with the origin centred on the soma, where the RW starts, and the angle initial condition set to zero:
\begin{gather}
	\vec r_n - \vec r_{n-1} = \hat{x} \cdot \rho \cos{\theta_n} + \hat{y} \cdot \rho \sin{\theta_n}
\end{gather}
The step is generally not fixed, but the speed is. Generally speaking to compute the evolution only the stochastic variable $\theta$ is drawn.\\
\subsection{Geometrical requirements for the RW algorithm}
Outgrowing GCs are constrained to go straight-forward from the rigidity of micro-tubules. This simple argument exclude the possibility of drawing the angle from an isotropic distributions, as the uniform distribution over $2 \pi$.
The next step direction has to be influenced by the previous in the angles variable space, the process, where coordinates have a temporal correlation, is also known as Brownian motion.
We can set whichever functional form to determine the walker stochastic evolution but we started from the Gaussian.
Such idea relays on some general results from cultured neurons' experiments~\cite{renaud_thesis}; and a micro-physic explanation of such behaviour is based on simple statistical mechanic system fluctuating around the minimum energy.
Let's consider the micro-tubules stiffness $k_{mt}$ and the energy required to bend the neurite with a certain angle, looking at the steering as a fluctuation from the equilibrium:
\begin{gather}
	\Delta E = \int_{\theta_0}^{\Delta \theta} k (\theta-\theta_0)  d\theta = k \Delta \theta^2 \\
	P(\Delta \theta) \sim e^{- \Delta E \beta_{eff}} \sim \exp({- \frac{\Delta \theta^2}{2} \beta_{eff}})
	\label{eq:elastic_gaussian}
\end{gather}
Where $\beta_{eff}$ collects all the system chemical and thermal details and is interpreted as the variance $\sigma$.
Other dynamics, with a more robust theoretical background, as those leading to Levy-flights or power-law distributions are presented in~\cite{Metzler2004}. To study the Gaussian model and check its results is an useful work, at least to show its limits.\\
Let's recall briefly the growth cone we have described so far, the factors we want to take into account are the following:
\begin{itemize}
	\item \textbf{the stiffness of micro-tubules} Neurite's microtubule core prevents the system from abrupt turning.~\cite{Martin2013}.
	\item \textbf{the soma-tropism of neurite}: Neuron's biological objective is to reach out other neurons. The neuronal targets are reached by the growth cones which undergo self-referential forces preventing from self-rolling and back-turning.~\cite{Memelli2013}
	\item \textbf{Space isotropy}: The growth cones haven't any spatial preference in an isotropic environment and everything is symmetric respect the extrusion angle.
	\item \textbf{Ballistic assumption}: The measures performed on real
	      neurons show ballistic behaviour, the GC directs straight-forward
	      away from the soma.
\end{itemize}
Furthermore we have to consider the requirements set by the simulator itself:
\begin{itemize}
	\item  The computational cost must not increase with the distance from the soma.
	\item The algorithm has to be expanded to an environment interaction model.
\end{itemize}
Furthermore we define a physical property, the persistence length $l_p$.
\paragraph{Correlation and Persistence Length}
The correlation in 2D can be defined through a vectorial picture of the process:
Let's $\vec{b_i}$ the element of the trajectory $\gamma $ which the segment ${i,j,\ldots,n}$ belong to. Let's each step has the same length $b$.
The correlation between the segments is:
\begin{equation}
	C(i,j) = \frac{\langle\vec{b_i} \vec{b_j}\rangle}{b^2} = \langle
	\cos(\Delta \theta_{i,j}) \rangle  \label{eq:cosine}
\end{equation}
Assuming the process in a stationary state, the correlation function will depend only by the distance $s = i - j$, whether this is not the case we set the site $i$ constant to zero and $j$ stays the only variable. Hence, developing the function around $ \Delta \theta \sim 0$:
\begin{gather}
	C(s) = \langle \cos(\Delta \theta_{s}) \rangle =  \sum_{0}^{\infty} \frac{(-1)^n}{(2n)!} \langle (\Delta\theta_s)^{2n} \rangle
	\notag
\end{gather}
while $\Delta \theta_s$ remains close to zero, the
expansion can be reduced to the
quadratic term.
\begin{gather}
C(s) = 1 - \frac{1}{2}\langle (\Delta\theta_s)^{2} \rangle \label{eq:cosine_linearization}
\end{gather}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{images/persistence}
	\caption{\textbf{Persistence length of dissociated microtubules.}The
		persistence length can be calculated from mature cultured neurons
		\textit{in vitro}. Measures the persistence length of dissociated
		microtubules can help to forecast the relation between the neurite
		diameter and its stiffness. This article is particularly relevant
		because implement a method to measure the persistence length for
		fixed microtubules.
		The pictures report results from the experiment documented in~\cite{Martin2013}.
		(A) TIRF image of microtubules sparsely decorated with fluorescent GTP analogs. Scale bar is 5 mm. (B) Microtubule trajectories from (A). (C) Average of $\cos(\Delta\theta_s)$ as a function of path length, $s$. The data shown are an average across all trajectories in (B). Fewer independent data for long trajectory lengths lead to statistical fluctuations. \textit{The persistence length for these microtubules is 150 $\mu$m}}
	\label{fig:persistence_mt}
\end{figure}
In this picture of the process the correlation is still an abstract quantity,
it does depends by the time interval and misses a characteristic length. It is
way more useful to define a characteristic length that can be obtained
measuring mature neurons, an experimental measure of microtubules persistence
length is showed in Fig.~\ref{fig:persistence_mt}. The persistence length should be invariant on coarse graining of the trajectory, while the correlation between two segments distant $s$ time intervals changes when the time step is augmented or reduced. \\
Such characteristic length is a general property of local processes in physics, while it disappears in phase transition and long range correlations: the functional form of correlation depends by the recursive equation, it is exponential decaying for short range phenomena and power law for long range.
The algorithms we will use belongs to the first class and thereafter we can
define the persistence length, regarding to real neurons this cannot be said,
because some process, i.e.\ self-avoiding walks, lacks a characteristic length
and the correlation decay slower. Whether the real neurons should belong to the
latter category different algorithms will be implemented.
To characterize the neurite properties the persistence length is a precise
tool. It is expected to be a macroscopic property of the system, the average
length of a straight segment originating in the soma. The persistence length
depends by the parameters of the problem and is a property of the physical
system, which has a well defined space-scale.
The persistence length can be obtained placing the function $C(i)$ in the real space. Assuming the system varies continuously with a function $\bar x(t)$, we have some points of the curve, the resolution doesn't affect the problem since the scale is fixed. The variable $s$ is the length of curvilinear path, that is the length of the dendrite itself. The variable $s$ is measured in $\mu m$ and the persistence length is defined as:$l_p= min \{s: C(s) < e^{-1}\}$
\begin{equation}
	C(l)= \exp(- \frac{l}{l_p})
	\label{eq:correlation_exponential}
\end{equation}
\subsection{Choosing the appropriate algorithm, pros and cons of different descriptions}
Bearing in mind the properties outlined in the previous section we will describe some algorithms and analyse their
weakness and strengths.  A detailed characterization for the algorithm is offered in
Appendix~\ref{appendix:random_walk} here it is limited to maintain the unity of the manuscript.  There are two main
classes: Those implementing a stochastic equation for the angle variable; and those presenting an unified dynamics,
with the elongation speed influencing the directional dynamics directly. Some algorithms are compared
in~\ref{fig:run_tumble_parameters}.\\
The first algorithm on the stage is the Correlated Random Walk, CRW\@. The dynamics is simple and is governed only by the
following equation, $r$ represents a correlated Gaussian variable, with unity variance and zero mean:
\begin{gather}
	r_{n} = \beta \cdot r_{n-1}+ \sqrt{1-\beta^2} \cdot \xi_{n-1}   \label{eq:crw} \\
	\theta_{n+1} = \theta_0 + (r_{n}) \sqrt{\sigma} \label{eq:rw}\\
	r_{0} = \xi_0 \notag \\
\end{gather}
Where $\xi_n$ is a Gaussian distribution with mean zero and variance one.
The angle $\theta_0$ is set and the neurite produce a river like path around its direction.
The persistence length of CRW is set by the parameter $\beta$ for the angle variable, while the neurite does not experience turns.
This algorithm produces a naive behaviour, the GC outgrowth and keeps going straight for all the growth process, whether there is a resemblance for some neurons' sample it is not usable in the NetGrowth simulator. Indeed it depends by the initial conditions, whether this is fine for free space neurons, those constrained by the environment will present bizarre behaviours.\\
A plausible correction for the CRW is to set dynamically the angle $\theta_0$, avoiding the crucial problem described above.
The new algorithm presents a short term memory that variates the direction, the equation for the Memory CRW follows, while a descriptive picture is offered in Fig.~\ref{fig:memory}
\begin{gather}
	\bar \theta_n  = \frac{\sum_{i=0}^{n} \alpha^{n-i} \theta_{i}}{\sum_{i=0}^{n}
		\alpha^{n-i}} \label{eq:memory}
	\notag\\
	r_{n} = \beta \cdot r_{n-1}+ \sqrt{1-\beta^2} \cdot \xi_{n-1}  \notag\\
	\notag\\
	\theta_{n+1} = \bar \theta_{n} + (r_{n}) \sqrt{\sigma} \notag \\
\end{gather}
Initialized with:
\begin{gather}
	r_{0} = \xi_0 \notag \\
	\bar \theta_0 = \theta_0 \notag
\end{gather}
The term~\ref{eq:memory} averages over the trajectory: takes into account previous directions and allows the random walker to variate its original direction. Memory acts as a rotation and doesn't take part to the stochastic computation. It is parametrized with $\alpha$ and dominates the persistence length.
Parameters $\alpha$, $\beta$ and $\sigma$ cannot be chosen in the whole space or pathologic behaviours will appear:
i.e.\ the correlated Gaussian cannot be larger of memory constraint or the walker will rotate on itself. They are not
orthogonal, as it will be shown later, and it's not possible to map one-to-one to stiffness, soma-tropism, etc.
This model fits better for the NetGrowth scopes and it has been implemented in the simulator, whether it cannot be related easily to the biology of the microtubules its worst drawback is another.
The algorithm presented leads to very complex discrete differential equations,
which cannot be solved (the detailed computation is in the
Appendix~\ref{appendix:random_walk}). Hence the persistence length cannot be
obtained analytically by the parameters. This won't be a problem if a numerical
estimation could be performed, but the algorithm produces 2D processes whose
persistence length changes in a super-exponential fashion in respect to model
parameter; it becomes very hard to fit via the three different parameters, as illustrated in Fig.~\ref{fig:MCRW}.
This annoying properties, added to its non linear dependence by the resolution of time step (indeed for higher resolution the number of turns is reduced abruptly) which cannot be correct with multiplicative factors, makes the MCRW very hard to pass the "reproducibility" test. Whether it can produce interesting shapes as showed in Fig.~\ref{fig:randomwalkaxonconfront}, it is not suited to predict neuronal network from biological data.
\begin{figure}[p]
	\centering
	\includegraphics[width=0.7\linewidth]{images/memory}
	\caption{\textbf{Memory constrained algorithm}. The algorithm sums previous versors with a dumping factor $\alpha$.
		The growth cone rotates and aligns itself with the angle weighted over the trajectory. The fluctuation due to normal distribution are attenuated and the correlation remains high despite the process has a large variance $\sigma^2$.
		In the figure the red semicircle and arrow represent the direction of last segment, the green circle is obtained with the memory algorithm instead.}
	\label{fig:memory}
\end{figure}
\begin{figure}[p]
	\centering
	\includegraphics[width=0.5\linewidth]{images/random_walk_axon_confront}
	\caption{\textbf{Neurons with different persistence length.} In the upside plots a set of 4 neurons is growth with different parameters for the random walk, the branching model is Van Pelt and is discussed further in next sections. The persistence length decreases from 1 to 4.
		The adimensional parameters for Gaussian correlation and memory coefficient are set equal for simplicity. Plot assumes respectively values 0.7, 0.6, 0.5, 0.4.}
	\label{fig:randomwalkaxonconfront}
\end{figure}
\begin{figure}[p]
	\centering
        \includegraphics[width=0.7\linewidth]{random_walk/sigma_memory}
	\caption{\textbf{Persistence length is super exponential for MCRW} The picture shows the dependence of the measured persistence length for the MCRW algorithm, the presence of memory makes the algorithm much harder to approach, even numerically.
		In log-scale for the ordinate, the persistence length appears super-exponential over $\alpha$. Different colors represent different values of $\sigma$ }
	\label{fig:MCRW}
\end{figure}
The impossibility to produce sensible behaviours with such simple algorithms constrained us to rethink the whole stochastic process.
The first interest remains to produce a persistence length $l_p$ that can be set by the user \textit{before} the simulation, maybe from data obtained in neurobiology laboratories.
The complexity showed by the MCRW requires to make a step backwards toward a more predictable algorithm: The simplest case we can imagine is the Diffusion RW, as the name suggest the angle variable changes with a simple diffusive stochastic equation, and is governed only by the variance parameter $\sigma$:
\begin{gather}
	\theta_{n+1} = \theta_0 + \xi_{n} \sqrt\sigma  \label{eq:diffusive_rw}
\end{gather}
In this case the angle shows a completely diffusive behaviour and, as it is showed in the appendix, it leads to a
double scale dynamics for the GC in the $xy$ plane. For a scale inferior or comparable to the inverse of the Diffusion
coefficient the GC presents ballistic pace, for larger scales the walk is diffusive. Assumed the elongation rate
constant, $v$, a constant turning rate  $\nu$ can be defined as a funtion of the time resolution $\Delta t$:
\begin{equation}
	\nu  = \frac{1}{v \cdot \Delta t} \\
\end{equation}
$\nu$ it is the number of rotation done by the GC in $1 \mu m$.
The diffusion process for the angle impose a linear equation for the Mean Square Displacemennt of the angle variable:
\begin{equation}
	\langle \Delta \theta^2 (l) \rangle = \sigma \nu l \label{eq:diffusion}
\end{equation}
From Eq.~\ref{eq:correlation_exponential} and Eq.~\ref{eq:cosine_linearization}, looking for distance $l$ much shorter than the persistence length:
\begin{align}
	C(l) & = \exp(-l / l_p) \sim 1 - \frac{l}{l_p}                                                 \\
	C(l) & = 1 - \frac{1}{2} \langle \Delta \theta^2 (l) \rangle = 1 - \sigma \nu l \label{eq:clt} \\
	l_p  & = \frac{2 l}{\cdot \langle \Delta \theta^2 (l) \rangle}
	=\frac{2 v_{GC} \cdot \Delta t}{\sigma} \label{eq:pers_analytic}                               \\
\end{align}
Eq.~\ref{eq:clt} is nothing else than the central limit theorem. By Eq.~\ref{eq:pers_analytic} the persistence length is analytically computable.\\
It is possible to verify its consistency measuring the persistence length with Eq.~\ref{eq:cosine}, which confirms the exponential decay.
Fig.~\ref{fig:diffusive_4plots} resumes the results.
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/rw/four_models}
	\caption{\textbf{Correlation
        and contraction measures for
different models} A set of similar
statistical measures  is applied to
navigation models presented. The colors
stand for different models:\\
Light blue is simple diffusive RW, Red
is the self referential force model
proposed in \cite{Memelli2013}, green
is the Run and Tumble and dark blue is
the MCRW devised by us.\\
\textbf{A} The contraction rate is the
ratio between the distance from the
soma and the length of the neurite, it
converges in diffusive regime as
discussed in the
Appendix\ref{appendix:random_walk}.
\textbf{B} The \textit{xy} MSD says the
caverage distance from the soma as a
function of length, when the regime
moves from balistic to diffusive it
changes from quadratic to linear.
\textbf{C} Shows the correlation
between segments direction, it is a
numerical implementation of
Eq.~\ref{eq:cosine}, it goes to zero
when diffusive regime starts. The
persistence length can be obtained by a
tfit of this measure. Eventually
\textbf{D} present the angle variable
MSD, it diverges very slowly in UTR and
MCRW, while surpasses $\pi$ in few
steps in other models. The angle
variable diffuses in all the models and
the slope should reveal the persistence
length also, the slope is related to
the \textit{$D_{coeff}$}. Detailed explanation of methods in the Appendix~\ref{appendix:random_walk}. }
	\label{fig:diffusive_4plots}
\end{figure}
The picture of the process in Eq.~\ref{eq:diffusive_rw} is very clear, but the drawback of this method appears
immediately when implemented: in order to have persistence length consistent with biological measured ones, the value
of $\sigma$ must be very little, i.e.\ less then $0.1 rad$.
This becomes a relevant problem when upgrading to environment interaction: the GC interacts with the walls in a broader
angle, around 180\degree, whether the walls interact as a potential on the angle distribution it is necessary that the
probability of turning in the wall direction is more than one. This is to say in such model the GC will be blid to the
walls except for a narrow angle.
\paragraph{Run and tumble}
To solve this issue we have followed the method used in NetMorph simulator: the turns are diluted in time with an
exponential distribution. In the fashion of a run an tumble model.\\
The growth cone, has a certain probability to turn, depending only by the length of the step: $ P_{turn}(l) =
	\frac{l}{\lambda}$.
This model is called uniform turning rate (UTR) and produces straight intervals, with mean $\lambda$ and exponentially
distributed. In this case the coefficient $\nu = \frac{1}{\lambda}$ depends by the turning rate strictly.
It is necessary to stress we are talking of rate in terms of spatial units.
\begin{gather}
	P_{straight}(l) = \frac{1}{\lambda} \exp(-l/\lambda) \quad E\left[l\right] = \lambda \quad Var\left[l\right] = \lambda^2 \notag \\
	l_p  = \frac{2 \lambda}{\sigma} \label{eq:UTR} \\
\end{gather}
With the last equation the model was largely enhanced: the persistence length is now independent by other quantities, like growth cone speed or resolution time, and can be fixed, case by case, by the parameter $\lambda$.
In Fig.~\ref{fig:turningratepicture} the linear dependence of the persistence length is shown for variation of both $\sigma$ and $\lambda$.
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/rw/turning_rate_picture}
	\caption{\textbf{Persistence length as a function of turning rate} In this two pictures we verify the linear equation for the Uniform Turning Rate model~\ref{eq:UTR}.
		Focusing on the concept of turning rate shifts the attention from the speed and the time resolution, to the
		actual spatial realization of the process: measuring the turning rate in micrometres instead of second is a robust
		definition that is invariant in respect of the resolution and the time-step; even it is well defined for neuron images.
		\textbf{A} shows the persistence length measured for varying parameters $\sigma$ and $\lambda$. The persistence length is measured as the ensemble average of $\langle cos (\theta)\rangle$. The data are produced by the NetGrowth simulator itself.\\
		In \textbf{B} the angular coefficient obtained above is plotted over the expected value with an excellent precision. The plot confirms that for the UTR algorithm the effective persistence length can be arbitrarily set by $\theta$ and $\lambda$.}
	\label{fig:turningratepicture}
\end{figure}
\subsection{Methods an Validation}
To study the algorithm a vast amount of experiments with single growth cones was run, they make up a statistical
ensemble. Hence the percentile distributions were computed and the fits too.
To study real neurons we have looked at
NeuroMorpho\href{http://www.neuronland.org/NLMorphologyConverter/MorphologyFormats/SWC/Spec.html}{NeuroMorpho database}
database, we have used rat retinal neurons since any other two dimensional neuron was available and there weren't
SWC files of cultured samples. To measure their properties we have decomposed each branch tree into single paths and
computed ensemble average. \\
This approach has some inner limits: study stationary properties of  non equilibrium process is hard, even more if the
dynamics is lost and a 2D static image is the only track of the process. Furthermore the axon is just one and was
impossible to study its statistical properties on the reduced set we disposed. With the dendrites we were luckier and a
complete characterization was performed.\\
Unfortunately the neuron growth in cultures are not on the database, which concentrates on \textit{invivo} samples: to
study 2D neurons we had to adoperate neuron from the retina of rats. These neurons are slightly different from those
present in neurons cortical area, used for the cultures. Anyway a sophisticated tool to reconstruct the neurite from
the SWC file has been done and can be used in future with effective data. The principal measure remains the persistence
length that can be measured from mature neurons or even in laboratory with microtubule dissociation as evinced in Fig.~\ref{fig:persistence_mt}.
Some of the analysed neurons are
reported in Fig.~\ref{fig:realneuron}
\begin{figure}[p]
	\centering
	\includegraphics[width=0.8\linewidth]{images/rw/retinas}
	\includegraphics[width=0.8\linewidth]{images/rw/retina_rw_neurons}
	\caption{\textbf{Characterization of dissociated neurons}
		A set of neurons from
                NeuroMorpho database
                are measured to extract
                the characteristic
                persistence length. The
                images, in SWC format
                (see
                Appendix~\ref{appendix:swc})
                have been processed and
                reconstructed to
                analyse the property of
                the whole neurite. Ech
                neurite has been
                decomposed in shorter
                and longer branches and
                the last one have been
                averaged for the
                salient measures. The
                neurons plotted are two
                of the analysed ones.
                Unfortunately
                NeuroMorpho has not 2D
                reconstructions, hence
                we had to use retina
                neurons and not
                cortical cells or
                interneurons.
		In the lower set of
            pictures we reproduce the
        standard measure to calculate
    the persistence length, this is
attested around  50 micrometers. We
cannot affirm this measures are sound,
indeed the persistence length appears
to be much longer and the diffusive
regime far. Hence we cannot say whether
or not Random Walk regime will
effectively appear:
In picture \textbf{C} the correlation
results strong after 40 $\mu m$ and we
cannot predicts it will go to zero or not. }
	\label{fig:realneuron}
\end{figure}
\subsection{Conclusion}
The analysis of the navigation mechanism for the growth cones, and the simulation of it, has required a deep insight into bi-dimensional stochastic processes. The properties required for the simulator have been outlined in the first section and the algorithms have been evaluated over them.
Eventually a modified diffusion process has resulted enough simple to be analytically solved still meaning full for the scopes.
The benchmark of the model are cultured neurons, but relevant data were not found on the Web and so a precise verification is postponed.
In Fig.~\ref{fig:run_tumble_parameters} some neurons built with UTR model varying the rate parameter are plotted.
Further model for environment sensing and direction election will be studied in future, a particular attention will be reserved to self-avoiding walks.
\begin{figure}
    \centering
	\includegraphics[width=0.7\linewidth]{images/rw/run_tumble_confront}
	\caption{\textbf{Varying parameters for the UTR model}
		In the pictures from 1 to 4 a neuron is generated with an increasing persistence length. \\
		The simulator can reproduce a vast amount of neuronal structure, varying the branching rate, the speed and the persistence length. The user who wants to simulate a precise neuron has to fit his experiment or find data of the type of neuron he wants to simulate.}
	\label{fig:run_tumble_parameters}
\end{figure}
\begin{figure}
    \centering
	\includegraphics[width=0.9\linewidth]{images/rw/density_path_compare}
	\caption{\textbf{Density distribution for different models of GC navigation}
	        To choose an appropriate model for the growth cone navigation we have evaluated the outcome of some
                \textit{classical} algorithms and created a new one. In pictures
                we show the average path, and a sample path in red, of the outgrowing neurite.
                Pictures \textbf{A} and \textbf{B} appear similar, yet the former (UTR or run tumble) is resolution
                invariant and the
                latter (MCRW or persistent random walk) is not. The MCRW is governed by three parameters, that makes very hard to
                predict its behaviour for a changing time resolution.
                Picture \textbf{C} reproduces the algorithm proposed by~\cite{Memelli2013}, although it is demonstrated it
                produces relevant morphologies it has four parameters which are not directly related to a possible
                experimental measure, also the
                algorithm was very hard to tackle analytically and to predict its time-resolution dependence,
                furthermore it is not a generalized Random Walk and it is not sound to speak about persistence length
                in this case.
                Eventually in \textbf{D} a simple random walk is illustrated. This process does not produce plausible
                morphologies: it is regulated only by the diffusive coefficient, if it is very low the growth cone goes
                straight and ignore the surroundings, other way it turns and self-crosses repeatedly.}
                \label{fig:run_tumble_parameters}
\end{figure}
%
%\begin{figure}
%	\centering
%	\caption{}
%	\label{fig:pcolor}
%\end{figure}
%\begin{figure}
%	\begin{center}
%		\includegraphics[width=0.7\textwidth]{images/memory_graph}
%	\end{center}
%	\label{fig:memory}
%	\caption{Memory constrained walker. Former picture presents a set of experiment with fixed variance and variable $\alpha$. The latter shows the fits for a set with variable memory and angle of view. In this case too the most of the measures are well fitted with a power law. In the position MSD the knee reveals the switch from ballistic to diffusive behavior.}
%
%\end{figure}
%
%\subsection{Evaluation of Netgrowth}
%Let's
%\subsection{Remarks}
%The random walk is the baseline method for many process in physics and biology. Netgrowth implement an enhanced algorithm to keep the variance as large as measured in Renaud experiments and maintain a long (hundreds of step) persistence in direction.
%We govern the process with two parameters, one for correlate the gaussian step by step, the other to realign the angle with previous direction.
%In Fig.~\ref{memory_vs_pers} the impact of these two factors is illustrated.
%In order to tune the simulator on a certain type of neuron two solutions are possible:
%\begin{itemize}
%	\item Retrieve the parameter for correlation and memory from biological models, this way is delightful but requires a better comprehension of effective forces inside the growth cone.
%	\item Study the real neuron with calcium imaging or other inspection techniques and then measure its statistical properties (tortuosity, msd, etc...) and match them with the relative parameters.
%\end{itemize}
%
%
%
\end{document}
