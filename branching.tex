% !TeX root = branching.tex
% !TeX spellcheck = en_UK
\documentclass[main.tex]{subfiles}
\begin{document}
\chapter {From single Growth Cone to Neurites}
\minitoc
Neurons connect each other through growth cones, and these connections have to be multiple to turn on the complex behaviour of brain tissue. We have outlined the strategies for this objective are many, but branching is the most widespread for all the neurons. On the other hand, from the descriptive point of view branching is the \textit{de facto} nature of neurites: a biologically relevant simulator must reproduce this mechanism as close to the real phenomena as statistic measures can evaluate.
The branching itself leads to more complex processes that affects the whole neurite: competition, fasciculation, retraction and pruning etc...
The wide variety of these events and mechanism prevents
from an accurate description of the arborization, but it's
possible, as generally is done in physics, to stress the
relevance of some aspects over the others. In a more abstract way we would say that the best model is the simplest one that is enough complex to describe the actual phenomena, all further complications are avoidable and can cloud the interesting behaviours.
An example of such choices is the following: in the first stage of Netgrowth realization we decided to describe the Actin Waves, an extraordinary phenomenon that shapes the neurite initiation phase, after a while we understood the minor relevance of this in branching formation, and preferred to remove from the simulation.
We chose to implement those mechanism that can reproduce a
complex arborization as the one showed in
Fig.~\ref{fig:Chandelier}.

First the Growth Cone is enabled to split, the split is
forced to respect a volume conservation law. The neurite
can also branch laterally and originates a new GC.
Eventually GCs compete each other and while some elongate
some other retract. When retraction is continuous the
branch is eventually pruned.\\
This chapter is divided in three sections: the branching mechanism, in terms of events' probability is described in the
first section, here we present an adaptation of Van Pelt model for arborization, and a uniform branching model for
lateral branching. The second section introduces the diameter to compute branching angles and branches' diameters.
Once the neurite with multiple branches
is introduced it is important to
predicts their elongation rate, the
speed of growth cones; this aspect is
tackled in the third section, here we
introduce a brand new model for
critical resource based competition, it
is defined and studied analytically.
Eventually the produced neurons are evaluated with standard measures for neuronal morphologies and confronted with other academic results.
\section{Modelling branching processes}
Here the NetGrowth algorithm for
branching events are accurately
described, first the Growth Cone
splitting, and later the shaft
branching. I have not produced an
original model in this case, rather I
adapted a successfully tested one to
implement it efficiently in NetGrowth,
maintaining the relevant physics
behind.\\
Let's introduce briefly one of the
simulation constraints that influenced
the implementation of both these
models: A general request for the simulator is
to reduce the operations wherever is
possible, even more to produce as few
stochastic variables as possible; this
constraint is particularly relevant
in case of branching with BEST model.
When the occurrence of branching is
governed by a rate at least two approach are
possible. A naïve
method suggests to compute the branching
probability and check
whether it happens, for each step;
that is to generate a random variable and
test it is less then a certain
threshold.
In such a scenario the number of
operation grows with the number of
growth cones and, even worse, is influenced by the resolution
time. A valid approach, instead, consists
into switching from the branching rate to
the interval distribution,
pre-computing the time next event
should happen. Hereafter to
predict the branching event and apply
it only at the forecasted
time-step saves
a huge quantity of computers' clocks.\\
In the following studies the rate
depends on the state of the neurite
only,
i.e.\ the number of growth cones, the
age of the neuron, and so on, hence the
interval distribution can be
precomputed for each neurite given the
state will not change during the
interval itself, e.g.\ recompute it
when a new growth cone rises for
other model or disappears for pruning.
With this approach we can reproduce exactly the BEST model~\cite{Pelt2003a} and whichever branching mechanism whose interval
distribution is known.
For the interstitial branching a similar approach is pursued.
\subsection {Growth cone splitting, reversing Van Pelt distribution}
To estimate an interval distribution consistent with BEST model\footnote{One of most cited branching model, it is a
	fully phenomenological model whose parameters can be obtained from statistical measure of mature neurons}
introduced in Eq.~\ref{eq:vanpelt}, we have started from the branching rate:
\begin{gather}
	p_{i}(t|n(t)) = \mathcal{N} n^{-E} 2^{-\gamma_i S} D(t) \label{eq:vanpelt} \\
	\text{with the normalization factor}\\
	\mathcal{N^{-1}}= \sum^{n(t)} 2^{\gamma_i S}
\end{gather}
where $p_{i}(t|n(t)$ is the rate of \textit{i}th Growth Cone splitting (namely the transition rate from a state with $n$
to $n+1$); the function $D(t)$ is a baseline branching distribution, exponential decreasing, determined by
experimental data: this distribution states for the diminished probability of observing a branch event in mature
cultures, from~\cite{Pelt2003a} we set $ D(t) = A \exp(- t/\tau)$.
The other parameters are $E$ which define the competition
among growth cones and $S$ which accounts for tree asymmetry~\footnote{For a tree $\alpha$ of degree $n$:
	\begin{equation}
		A_{p} \left( \alpha^{n}\right)=\frac{1}{n-1} \sum_{j=1}^{n-1} A_p(r_{j},s_{j})
	\end{equation}
	is the mean value of partition asymmetries in the tree, which, at each of the $n$ bifurcation points, indicate the relative difference in the number of terminal segments $r_j$ and $s_j$ in the two sub-trees emerging from the $j$th
	bifurcation point.
	The partition asymmetry $A_p$ at a bifurcation is defined as:
	$$A_{p}(r,s)=\frac{|r-s|}{r+s-2}$$
}, the last one is less important for this description because the normalization factor ensure the branching rate
independent by the parameter $S$.
The average number of tips, i.e. GCs, in the neurite is governed by the differential equation:
\begin{equation}
	\frac{dn}{dt} = n(t)p_i(t|n(t))
\end{equation}
We can integrate Eq.~\ref{eq:vanpelt} over all the possible segments and obtain the total rate of branching $p(t|n(t)$ as:
\begin{gather}
	p(t|n(t)) = \sum^{n(t)}_i p_i(t|n(t)), \gamma_i) = D(t) n(t)^{-E} \label{vanpelt_rate}\\
	\frac{d\langle n \rangle}{dt} = \langle n(t) \rangle p(t|n(t))= D(t) \langle n \rangle ^{1-E}(t) \label{vanpelt_diff}
\end{gather}
And set the boundary condition $n(0) =1 $ which corresponds to start with one Growth Cone.
The Eq~\ref{vanpelt_diff} can be solved as a separable first order ODE and we can explicit the $ \langle n(t)
	\rangle$ as a function of time and branching parameters $A,\tau,E$
\begin{equation}
	\langle n \rangle(t) = (\frac{AE(1 -e^{-t/ \tau})}{c}+1)^{1/E} \label{eq:average_br_events}
\end{equation}
$\langle n(t) \rangle $ is a continuous, real, variable accounting for the average number of growth cones. Its
evolution depends by for varying the competition parameter is presented in Fig.~\ref{fig:n_varying_E}.\\
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/branching/vanpelt/n_vaying_E}
        \caption{\textbf{Expected number of branching events} Varying the competition parameter the number of growth cone in the final tree changes, this dependence agrees with the competition model presented lateron. The number of branching events is regulated by the differential equation in Eq.~\ref{eq:average_br_events}. The picture presents the number of total branches expected.}
	\label{fig:n_varying_E}
\end{figure}
%The first drawback of this description is the variability it presents for a changing time step integration.
A formal approach we should compute the probability of having $n$ events, and then compute the average time between two events, given the parameters $\langle \Delta t \rangle(A,c,E)$ and its distribution. This process it is hard to study correctly, indeed the probability changes both in time and for the discrete component $n(t)$ thereafter a Fokker-Planck description would require a left-side repulsive barrier evolving in time. Anyway, the interesting distribution is not the $n(t)$ at the end of the growth but the events interval.
%\begin{align}
%p(t) &= -\frac{1}{(E+\frac{c}{A})\exp(	ct) -1 }\\
%\int p(t) dt &= \frac{\log\left(\frac{E c {\left(e^{\left(-c t\right)} - 1\right)}}{A} -	1\right)}{E c}\\
%\\
%C(\Delta t, t_0) &= \int^{t_0 + \Delta t}_{t_0} p(t) dt \label{cumulative}\\
%C(\Delta t, t_0) &= \frac{\log\left(\frac{{\left| -E c + {\left(E c + A\right)}e^{\left(\mathit{Dt} c + c t_{0}\right)} \right|} e^{\left(-\mathit{Dt} c - c t_{0}\right)}}{A}\right) - \log\left(\frac{{\left| -E c + {\left(E
%c + A\right)} e^{\left(c t_{0}\right)} \right|} e^{\left(-c
%t_{0}\right)}}{A}\right)}{E c}
%\end{align}
A simpler but less rigorous approach is to estimate the amount of time required to increase the number of GCs, given
$p(t | n(t) )$, the branching probabilities of the individual terminal segments per unit of time.
Let's proceed by step and assume $D(t)$ is constant in time, that is the rate of branching is uniform during the
growth, this rate, $\nu$, depends only by the number of growth cones and is constant in the interval between two branching
events. In the limit of a short interval $\Delta t$\footnote{$\Delta t \cdot \nu \ll 1$}, defined $P(X(t)=n) = P_n(t)$ with $X$ the number of branching events, the following equations hold:
\begin{gather}
	P_n(t+\Delta t) = P_0(t) (1-\nu \Delta t)\\\
	\dot{P_0}(t) = -\nu P_0(t)\notag\\
	P_0(t) = P_0e^{-\nu t}
\end{gather}
This distribution is the widely used in physics, and neuroscience, and it is an exponential decaying probability
distribution with mean value $\frac{1}{\nu}$. Up to here the interval distribution can be computed easily and is exact.
Let's see what happens in our case study which is explicitly time dependent.
The correct equation to solve, for the first event, becomes:
\begin{align}
	\dot{P_1}(t) = & -\nu(t) P_1(t)                                \\
	=              & -A e^{-ct}n^{-E} P_1(t)                       \\
	P_1(t) =       & P_1(0) \cdot  e^{-\int_0^{t} \nu(t) dt}e^{-1} \\
\end{align}
which becomes, for $P_1$(0) = 1:
\begin{equation}
	P_1(t)=e^{\frac{A}{c}( 1-e^{-tc}) }\label{eq:time_dependent_rate}
\end{equation}
The first relevant aspect of the Eq.~\ref{eq:time_dependent_rate} is the probability of having only one branch does not
disappear for large time. This fact can intuitively explained in this way: whether the baseline has a short
characteristic time, the rate decays so fast the any branching event can happen.
Therefore the interval mean time is:
\begin{equation}
	\langle \Delta t \rangle = \int_{0}^{\infty} t \mathcal{N} e^{\frac{A}{c}(1-e^{-tc})} \label{eq:exp_interval}
\end{equation}
For second and high orders interval the computation becomes tricky, indeed the baseline is an independent process. The
explicit time dependence prevents to compute interval distribution with simple methods, a correct computation should
comprehend the interval distribution of all previous extracted intervals. In principle, after $p_n(t)$ is obtained the
\textit{first return theory}~\cite{Parisi} can be used, hence first return probability $f(t)$ computed and eventually
the average interval and its distribution.\\
In order to make the computation feasible, and avoid a demanding analysis, the time dependence can coarse grained and
substituted with average values: \\
The methods can be outlined this way:
First, the rate defined in Eq.~\ref{vanpelt_rate}, is approximated by its value at the beginning of the interval,
$$D(t) \rightarrow D(t_0) = \exp (-t_0 / \tau)$$, this approximation is valid if the mean interval obtained this way is much
shorter then the characteristic time.
Second the problem is not solved in general but dynamically: we are, in principle, interested to the interval
distribution of the average process, but, to forecast the next event with the proper distribution is enough for the
simulator. Hence the number of growth cones is assumed as a parameter of the process and the interval distribution,
given $n$ events happened, becomes:
\begin{align}
	\nu(t_n,n) =                   & D(t= t_n) n^{-E}                                  \\
	\langle \Delta t (n) \rangle = & \int_0^{\infty} t \exp(t \nu (t_n, n)) dt \notag  \\
	=                              & A^{-1} e^{t_n/\tau}n^{E} \label{eq:time_interval}
\end{align}
where $t_n$ states the time of the $nth$ event. The constraint introduced above is respected for:
\begin{gather}
	\tau > \langle \Delta t_(n) \rangle\\
	\ln(\tau) > \frac{t_n}{\tau} + E \ln(n) + \ln(A) \sim \frac{t_n}{\tau}\\
	\label{eq:limit_approximation_vanpelt}
\end{gather}
This distribution was implemented in NetGrowth, the results are confronted with the BEST model in Fig.~\ref{fig:van_pelt_rate_vs_interval}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/branching/vanpelt/rate_vs_interval_e_04}
	\includegraphics[width=0.7\linewidth]{images/branching/vanpelt/rate_vs_interval_e_07}
	\caption{\textbf{Branching times for BEST model} Pictures \textbf{1} and \textbf{2} describe the
		statistic properties of
                topological tree
                generated by the BEST
                model. The histogram
                are produced running
                the algorithm $1000$
                times. This test verifies
                whether the
                approximated method
                reproduces the results obtained by the BEST model.\\
                Light blue
                histogram is generated
                with the fixed interval
                approximation (see
                Eq.\ref{eq:exp_interval}):
                the interval time
                distribution is
                computed and the
                branching time is drawn
                from it.
                Dark blue one trees are generated with BEST
                    standard algorithm
                    \textit{baseline}: each time
                    step it checks whether or
                    not to branch.
                In a separate study we
                have observed the time
                saved increases
                linearly with the
                number of expected branching
                events.
                The parameters of
                baseline $D(t)$ are
                equal for both the
                processes: $\tau =
                1000$, $A =1$.
                The matching of these two methods is tested varying the competition parameter: $E=0.3$ in \textbf{1} and $E=0.7$ in \textbf{2}\\
                The fixed time
                approximation proposed
                above fits adequately
                with the BEST model
                distribution, pictures
                \textbf{A}. The Inter
                Branching Interval
                distribution (IBI) in
                \textbf{B} and average
                time of branching
                occurrences are  well
                reproduced too. The
                approximation is weak
                for large interval, as
                explained in
                Eq.~\ref{eq:exp_interval}, and this results appear clearly in the third histogram.
		\label{fig:van_pelt_rate_vs_interval}}
\end{figure}
After the interval time is extracted the exact branch where the split happens is selected by a weighted choice basic algorithm. The weight depends dynamically by the branch order in the tree and by the symmetry parameter $S$ introduced above.
%The previous equation can be improved with a little effort, whether the rate is not constant in time we can in general consider the interval between two branching events as:
%\begin{align}
%1 =& \int^{\Delta t +t_0}_{t_0} d\langle n \rangle \\
%& \int^{\Delta t +t_0}_{t_0} D(t)  \langle n  \rangle(t)^{1-E} \label{eq:rate_n_constant}\\
%& \int^{\Delta t +t_0}_{t_0} \frac{(b(1-e^{-cx})+1)^{1/E}}{b (e^{cx} -1) +1}
%\end{align}
%Unfortunately, the equation became very complex and the solution has to be expanded in series. Which is not the scope of our approach. To simplify further it is necessary to assume the average number of growth cones $\langle n \rangle$ constant during the integral.
%Here the approximation becomes less rigorous but as illustrated in the end of the section the results correctly reproduces those reported by Van Pelt and collaborators.\\
%From Eq~\ref{eq:rate_n_constant}, integrating over the baseline distribution, the following expression for the expected interval is obtained.
%\begin{equation}
%\Delta t = - \frac{1}{c} \ln \left(
%\frac{\frac{A}{c}e^{-c t_0}} {n^{1-E}}-1
%\right) + \frac{1}{c} \ln \left( \frac{A}{c} n^{1-E} \right) -t_0\label{eq:vanpelt_deltat}
%\end{equation}
%It comes from the approximation that the expected time for each interval can be computed as the time necessary by the average number of Growth Cones to increase by one. This approximation is acceptable if the exponential decaying baseline $D(t)$ changes slowly in respect to the expected value $\Delta t$; the other parameters are constant, indeed, for all the realizations of the process.
%
%From  Eq.~\ref{eq:vanpelt_deltat} we can observe  $\Delta t$ depends by the logarithm of:
%$$\left( \frac{\frac{A}{c}e^{-c t_0}}{n^{1-E}} -1 \right)^{-1}$$
%Considering $t_0$ as the absolute time of last event the equation implies the time interval remains finite only when the baseline, computed in $t_0$ is greater then the intrinsic component of the probability. The divergence of time interval is reported in Fig.~\ref{fig:time_interval}.
%The divergence of the $\Delta t$ is actually welcome for the model scopes: the baseline was initially introduced to ensure the convergence of splitting events, i.e. prevent an infinite branching neurite. \\
%In order to implement the forecasting algorithm for GC splitting, not only the average value is necessary but its distribution too. The exponential distribution fits the scopes: it is memory less and its expected value is the rate itself. It is preferred to other distributions, e.g. the Gaussian, since the exponential is defined by one parameter only, the rate.
\subsection {Lateral (interstitial) branching}
For the lateral branching a similar approach is proposed following the results in~\cite{Szebenyi1998}.
Each segment has a certain probability
to generate a new growth cone, which
depends only by a local excess of
actin. These excess can be due to
fluctuations or growth cone pausing.
The new branch appears laterally and
starts a new growth cone. The GC so
generated is equivalent as it is appeared after a GC split event.
For a correct characterization and simulation of the process we investigate the explicit time dependence fo lateral branching events, and the spatial distribution of new GCs over the branch length.
The cited paper describes the process accurately and reports numerical results in terms of branching rate and distance from the leading growth cone, some relevant data are represented in Fig.~\ref{fig:lateral}.
Following the success reported in
previous section the same method is
applied: the rate is assumed to decay
with a certain function $$D(t) \sim  t^{-\alpha}$$, which can be
fitted by data. Hence the expected
interval $\Delta_t$
is computed assuming the rate constant
in and the branching
event time is drawn by an exponential
distribution (fixed rate assumption).
With this method the expected number of
branching events, in a certain
interval, is coarsely governed by the
value of $D(t)$ in the interval itself.
As above this method presents severe
flaws when the function $D(t)$ changes
rapidly in respect to the mean duration
of the interval $\Delta_t$. But this is
not the case for the data reported in
\cite{Szebenyi1998}.
The spot where the branch rises is
chosen by an exponential decaying
distribution, as explained in
Fig.~\ref{fig:lateral}. An example of
dendritic tree generated by this method
is presented in
Fig.\ref{fig:lateral_samples}.

\begin{figure}[p]
        \centering
	\includegraphics[width=\linewidth]{images/branching/lateral_neuron_dendro}
        \caption{\textbf{Lateral
        branching, neuron generated with
NetGrowth} The lateral events can occur
along the neurite length whenever an
amount of actin erupts from the neurite
shaft, in the picture I present a
neuron generated with NetGrowth
simulator. \textbf{A} is the neuron
with dendrites in blue and axon in
green. It presents only lateral
branching: the neurite elongate and
then branches with a powerlaw
decreasing rate. The neuron is
generated with run and tumble method
for the GC navigation and the
competition among Growth Cones is
switched off. As discussed the neuron
is generated precomputing the branching
time with a fixed rate approximation,
this approach saves huge amount of
random number drawings.\\
\textbf{B} and \textbf{C}
present the dendrogram for other
neurites simulated with the same
algorithm. In this case is very hard to
discuss the tree symmetry and
structure: the neurite branches
independently by the centrifugal order
of the tree. Only the length of the
branch matters: the probability of
branches decrease exponentially with
distance from the leading Growth Cone. Which of
neurites is going to ranch is chosen
weighting the branch length itself:
longer branches have more probability to
have an interstitial issue of actin.}
        \label{fig:lateral_samples}
\end{figure}
\begin{figure}[p]
        \centering
        \includegraphics[width=\linewidth]{images/branching/lateral4}
        \caption{\textbf{Lateral
        branching, experimental
evidences} The picture reports two relevant measures from cortical neurons in culture. (\textbf{A}) Shows a lateral branching event at 94h from culture initiation. Fig.s~(\textbf{B}) and (\textbf{C}) present a statistics of the observed neurons (58 samples). The former summarizes the frequency histogram, bars show numbers of new branches that extended during each of the time periods indicated. Most of the branches extended on the second and third day in culture.The blue, superimposed, histogram is the total number of interstitial respect to the time periods. The latter is the frequency histogram showing numbers of branches that extended from the axon at varying distances behind the primary growth cone. The functional form and the distribution were used to define the interstitial branching model. Data from~\cite{Szebenyi1998}.}
        \label{fig:lateral}
\end{figure}
\subsection{Conclusion}
The branching models for Growth Cone
splitting and lateral branching have
been adapted to minimize the
computational costs. The exact
estimation of the branching rates as
functions of parameters and number of
cones was hard to resolve exactly for
the explicit time dependence,
conversely a computational method was
introduce with great success in
reproducing the original model.
Some neurite generated with NetGrowth are presented and discussed in Fig.\ref{fig:models_tree}
\begin{figure}%
	\centering
	\includegraphics[width=0.7\linewidth]{images/branching/dendrogram-E_0_3-S_0_01.pdf}
	\includegraphics[width=0.7\linewidth]{images/branching/dendrogram-E_0_5-S_1_0.pdf}
	\caption{\textbf{Dendritic
            trees produced with GC
        split models in NetGrowth} The
        pictures present some of
        possible dendritic trees
        realized with the (modified) BEST
        algorithm. \textbf{A} and
        \textbf{B} graphs were
        produced varying the symmetry
        parameter: Once
        the
        branching time is computed with
        the fixed rate approximation
        (see
        Eq.~\ref{eq:limit_approximation_vanpelt})
        the election of
        splitting GC is made by the BEST
        model prescription: the
        parameter \textit{S} regulates
        the importance of centrifugal
        order, when it is greater then
        zero the tree is more likely to
        be symmetric. Also the
        competition parameter is varied
        , higher competition prevents
        cones from splitting.
        Neurite
        \textbf{A} has $S=0, E=0.3$, \textbf{B}
    has $S=1,E=0.7$.}
	\label{fig:models_tree}
\end{figure}


\section{Branching angles and diameter}
This short section will focus on the algorithm we implement to
produce relevant morphology in the respect of branching angles and diameters.
We have followed the data and models proposed in~\cite{Shefi2004}.
There are some constraints the splitting growth cones has been observed to
respect:
First there is a generalized \textit{Rall condition} the branching neurite
respects in absence of external cues, the diameter of the splitting growth
cones are governed by the simple equation:
\begin{equation}
    d_{father} = d^{\eta}_{1}+d^{\eta}_{2}
    \label{eq:Rall}
\end{equation}

Second, Shefi \&co proposes a model based on forces equilibrium, if the
branches are assumed to pull the bifurcation with a certain tension $T$,hence
the relation between diameters and branches angle results:
\begin{equation}
    \frac{T_1}{T_2} =\frac{\sin(\alpha_1)}{\sin(\alpha_2)}
    \notag
\end{equation}
where $\alpha_i$ is the angle of the branch in respect the parent branch, as
depicted in Fig.~\ref{fig:shefi_branching}.
The author proposes the pulling force to be proportional to some power of the
diameter, hence the resulting equation becomes:
\begin{equation}
    \frac{d^{\nu}_1}{T^{\nu}_2} =\frac{\sin(\alpha_1)}{\sin(\alpha_2)}
    \label{eq:tension_angle}
\end{equation}
To close the system we have to impose the branching angle $\alpha =
\alpha_1+\alpha_2$, which is derived by experimental observation.
The code in~\ref{code:angles} is the resulting implementation of these
simple constraints.
We have introduced a noise in Eq.~\ref{eq:Rall} to produce variable
morphologies, also we have assumed $\nu = 1$ in the absence of other
evidences.
In case of lateral branching we could not find any report, hence the diameter
has been produced with a power law decreasing width.
\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{images/branching/angles_shefi}
    \caption{\textbf{Angles and diameters, a mechanical equilibrium model}
    Following the experimental evidences reported in \cite{Shefi2004} I implemented a
    simple algorithm~\ref{code:angles} to reproduce the mechanical equilibrium model proposed
    in~\ref{eq:tension_angle},\ref{eq:Rall}. With this model the branches pull the
bifurcation with a strength proportional to their diameters. This hypothesis
is in agreement with the focal adhesion model, which predicts the pulling
action is exerted over the substrate by the molecular motors.}
    \label{fig:shefi_branching}
\end{figure}

\begin{lstlisting}[caption={Algorithm
for diameter-aware
branching},label={code:angles}]
    \label{code:angles}
    // draw the branching angle from a gaussian distribution as hypothesized in
    // reference article
    double branching_angle = gc_split_angle_mean_ +
                             gc_split_angle_std_ * normal_(*(rnd_engine).get());

    // ratio between the diameters of the two neurites,
    // it's a gaussian distributed value arround 1.
    double diameter_variance_ = 0.02; //@TODO here the variance was fixed,
                                      // requires to be set by the user!
    double ratio = 1 + diameter_variance_ * normal_(*(rnd_engine).get());
    // The diameters are computed on the base of:
    // d^eta = d_1^eta + d_2^eta
    // next two lines compute the new diameter
    // from the old diameter of the neurite and the ratio between the rising
    // cones.
    new_diameter = old_diameter * powf(1. + powf(ratio, diameter_eta_exp_),
                                       -1. / diameter_eta_exp_);
    old_diameter = powf(powf(old_diameter, diameter_eta_exp_) -
                            powf(new_diameter, diameter_eta_exp_),
                        1. / diameter_eta_exp_);

    // the diameter affect the branching angle: the largest cone goes straighter
    // than the other.
    double eps = (old_diameter - new_diameter) / (old_diameter + new_diameter);
    double half_tang = eps * tan(branching_angle / 2);
    new_angle        = old_angle;

    new_angle = -branching_angle / 2. - half_tang;
    old_angle = +branching_angle / 2. - half_tang;

\end{lstlisting}
\end{document}
