% !TeX spellcheck = en_UK
% !TeX root = models_intro.tex

\documentclass[tesi.tex]{subfiles}

\begin{document}
This part of the thesis will deal with the NetGrowth simulator. In the first chapter the biological process of neuronal growth will be described. Then we will recall the biophysical models in scientific literature and eventually discuss the models implemented in NetGrowth.

\chapter{Introduction to biophysics of neuronal outgrowth}

\minitoc
In this chapter the preparatory work done for the NetGrowth simulator is presented.
The chapter is divided in two sections. In the first we focus on the growth cone behaviour, looking at the interactions with the environment, the steering models, and the elongation rates. The second section gives a hint of the complex world of neurite's branching. \\
But first, a short description of neuronal growth and its several stages is offered before entering in the details of growth cone biology.\\

\paragraph{Neuronal growth stages}
Let's briefly describe the stages of neuronal outgrowth to familiarize ourselves with this very complex biological process.\\
During culture lifetime, neurons will extend their neurite and create synapses with other neurons. This process takes about 21 days.
At this stage of the project we are considering the neuron fixed on the substrate by the use of some adhesive proteins.

Neuronal growth occurs in several stages:\\
first, a \textit{neurite initiation} process occurs in which hand-like extensions of the cell membrane – the lamellipodia – develop into short neurites with a rich actin frontier. This process is enhanced by the actin-waves.\\
After a period of outgrowth and retraction of these newborn neurites, one of them develops the most and elongates rapidly over 2 days while inhibiting the outgrowth of the remaining neurites.
This predominant neurite becomes the axon and assumes distinct biological characteristics: the neurites now have \textit{differentiated} themselves into axon and dendrites.\\
The dendritic arbor comes out after a long period of \textbf{elongation and branching} of each neurite.
At first the arbor grows rapidly with respect to the total neuritic length, and the structure changes repeatedly. Eventually, the elongation rate slows down and the global shape of the arbor becomes stable.\\
The presence of obstacles, mechanical cues, or chemical ones drastically changes the shape and the timing of the arborization. The Growth Cones (GC) either show a repulsive or attractive behaviour with respect to different external stimuli. Such properties can be used to drive and control neuronal network formation.

\begin{figure}
	\centering

	\includegraphics[width=0.7\linewidth]{images/stages}
	\caption{	\textbf{Stages of neuronal arborization} The growth process is composed of  three main stages of neurite development: (1) initiation, (2) differentiation into an axon and dendrites, (3) arborization, including elongation and branching. The whole process lasts 3 weeks. Afterwards, the growth slows down and a reshaping phase initiates during which most of the dendrite is pruned. The topographical plasticity slowly disappears in favour of synaptic plasticity. }
	\label{fig:stages}
\end{figure}

\section{Growth Cone: searching for biological targets}
\begin{figure}
	\centering
	\textbf{Biological structure of a Growth Cone}
	\includegraphics[width=0.7\linewidth]{images/growth_cone}
	\caption{The structure of the growth cone is fundamental to its function. The leading edge consists of dynamic, finger-like filopodia that explore the road ahead, separated by Lamellipodia-like veils – sheets of membrane between the filopodia (see the figure). The cytoskeletal elements within the growth cone underlie its shape. The growth cone can be separated into three domains based on cytoskeletal distribution14. The peripheral (P)-domain contains long, bundled actin filaments (F-actin bundles), which form the filopodia. It also contains mesh-like branched F-actin networks, which give structure to lamellipodia-like veils. Additionally, individual, dynamic, and 'pioneering' microtubules (MTs) explore this region, usually along F-actin bundles. The central (C)-domain encloses stable, bundled MTs that enter the growth cone from the axon shaft, in addition to numerous organelles, vesicles and central actin bundles. Finally, the transition (T)-zone (also called T-domain) sits at the interface between the P- and C-domains, where actomyosin contractile structures – called actin arcs – lie perpendicular to F-actin bundles, forming a hemicircumferential ring within the T-zone33. The dynamics of these cytoskeletal players determine the growth cone shape and movement during its journey.
		Images from \cite{Lowery2009}}.
	\label{fig:growthcone}
\end{figure}

The Growth Cone is one of the most amazing actors in the outgrowth process of neurons.
It is the tip of the neurite. It senses the environment and assembles tubulin in micro-tubules in order to elongate towards its biological target. A simple sketch is offered in Figure \ref{fig:growthcone}.\\
To describe all aspects of Growth Cone elongation and retraction would require a long discussion. Moreover, no description would be completely exhaustive, since there are many diverging opinions. The interested reader will find a detailed review in \cite{Franze2010} and in  \cite{Lowery2009}.

Here, we will give an overview of the principal forces acting on the stage. The goal is to develop a simple model for the Netgrowth simulator.
Following the review in \cite{Suter2011}, we're going to present the recent studies on the elongation of axons, their response to traction, and the influence of the substrate on rate and direction of the elongation.
The approach will be bottom-up, starting from the vary basic components of the growth cones up to the physical and computational models  hopefully reproducing the elongation and the directionality of the growth cone.\\
For the following sections I thank Ian Audric, who has written a precise review on growth cone internal biology, for his work at IPGG in the group of Catherine Villard \footnote{http://neurofluidics.org/program/}.

\subsection{A pointed Little Head: biology of a growth cone}
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{images/actin-tubulin}
\caption{\textbf{Microtubule
polymerisation dynamics} Cartoon
representation of microtubule growth
and catastrophe. For a growing
microtubule (left), polymerisation
occurs most rapidly at the plus (+)
end through the addition and removal
of$\alpha-\beta$-tubulin heterodimers.
After incorporation into the
microtubule filament, hydrolysis of
the $\beta$-tubulin-bound GTP takes
place. End-binding (EB) proteins can
transiently bind to the plus end,
influencing polymerisation dynamics
both directly and indirectly by
recruiting other important regulatory
+TIPs. For uncapped filaments, slow
polymerisation and depolymerisation
can also take place at the microtubule
ebottomnd, most likely through similar
mechanisms as those occurring at the
upon end. A reduction in the concentration of free $\alpha-\beta$-tubulin heterodimers, for example due to stathmin-mediated sequestration, is one way in which microtubule catastrophe may be induced (right), leading to filament shrinkage. }
\label{fig:actin-tubuline}
\end{figure}
The growth cones are composed mainly by 3 regions, the \textit{Central}, \textit{Transition }and \textit{Peripheral} domains. The first domain is dominated by the presence of microtubules while the rest by \textit{lamellipodia} and \textit{filopodia},two actin filament structures. The macroscopic region containing the \textit{P} ans \textit{T} domains is called \textit{kinetoplasm}, or Growth Cone, while the \textit{axoplasm } connects the GC to the soma. \\
The equilibrium between these regions, the propensity to branch, the microtubule assembling rate and the direction are
regulated throw hundreds of
intracellular protein and
extracellular signals, sensed and
mediated throw the filopodia, the most
important process in this respect is
the turnover, that is
the process in which the protein are
polymerized into microtubules or actin
filaments or depolymerized into
molecular tubulin or G-Actin.
Let's get into details describing the main actors in the outgrowth process.

\textbf{The cytoskeleton } is a very dynamic network of protein filament. It is responsible for sustaining the structure of the cell. The growth cone is a particular arrangement of cytoskeletal filament, protruding from the main cellular body. \\
The neurite presents three main polymers: the F-actin, the micro-tubules, and the neurofilaments \cite{Coles2015}. The first two dominate the short time scale which we are interested in when looking at the dynamics of the growth cone.

\textbf{The actin filament} (F-actin) is a semi flexible polymer organized in a double helix structure. As explained in Figure \ref{fig:actin-tubuline}, the balance of polymerization and depolymerization is oriented. The actin is widely present in many cellular processes and it has the ability of building chains of forces. It is fundamental in processes such as cellular motility or mitosis. It can be organized in a dense network or in (anti) parallel bundles. See Fig~\ref{fig:growthcone}.\\
In the axoplasm F-actin is mostly organised into a cortical mesh around the microtubule’s inner core. The presence of myosin II motors in this cortex leads to the presence of contractile stresses \cite{Julicher2007}.
In the GC, F-actin is organised in a lamellipodium similar to the ones found in cells specialised in crawling.

\textbf{ The microtubules} (MTs) are rigid polymers of a diameter of around 25 nm composed of tubulin,ì – a proteic dimer. The tubulin is arranged laterally to create cylindric and hollow MTs.
The MT has a persistence length of the order of millimetre, as defined and measured in \cite{Martin2013}. Its stiffness gives a robust skeleton to the whole cell.
It oscillates continuously between a phase of elongation and retraction, also known as catastrophe. The active equilibrium is finely regulated by a protein called MAP (Microtubule Associated Protein).\\
They can be cross-linked by molecular motors (e.g. myosin II for F-actin, Dynein or Kinesin for microtubules) able to exert active stresses inside the meshwork.
Most of microtubules lie in the axoplasm and are connected by passive cross-linkers (MAP) that generate a network with a quasi-lattice structure. These microtubules are highly stable with turnover duration of hours, possibly thanks to the presence of MAPs.

The research is active on this topic and technological developments allow biologists to go further in the understanding of the complex behaviour of cytoskeleton polymerization and the roles of the many proteins active in the neurite filaments \cite{VonDerEcken2015114}.

\paragraph{Filopodia and Lamellipodia}

The growth cone contains an actin cytoskeleton which adds mechanical strength to the cone keeping its shape and as well helps driving and guiding the cone's movement. Filopodia are small actin filament bundles extruding from the tip of the growth cone. They grow and retract at a high rate constantly probing the environment and picking up guidance cues\footnote{ Many videos are available on \url{youtube.com}.}.
Filopodia can adhere to the substrate, producing tension within the growth cone~\cite{mogilner2005,Betz2011,Craig2012}.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/needle}
	\caption{\textbf{A biomechanic experiments to measure traction forces}
The growth cone produces different amounts of traction force during adhesion-mediated advance. (A) DIC images of a growth cone interacting with a 0.0025 N/m Con A-coated microneedle at the beginning of the experiment, at the end of latency and traction phases. (B) The same growth cone shown in (A) interacting with 0.0140 N/m Con A-coated microneedle. Needle stiffness (k), needle deflection, and traction force are indicated. (C)Kymographs showing the position of the microneedle throughout the time course of the experiments shown in (A) and (B). Arrows indicate the end of latency phase. Images from~\cite{Athamneh2015_Substrate}}
	\label{fig:tweezers}
\end{figure}
In \cite{Athamneh2015} the author offers a review over the many roles forces play during neurite outgrowth. How filopodia sense the environment is a relevant open question in this research field. Despite the lack of understanding of the mechanisms underlying mechanotransduction, experimentalists are using recent techniques – such as optical tweezers – to characterize the response of filopodia to external mechanical stimuli. Fig.~\ref{fig:tweezers} shows an experiment with a coated microneedle, in which experimenters measure the force exerted by the Growth Cone on a microneedle.

%\begin{figure}
%	\centering
%	\includegraphics[width=\linewidth, angle=270]{images/elongation}
%	\caption{\textbf{Stages of Growth Cone elongation}(a) Protrusion: F-actin is assembled along the leading edge and turned over by the actin flow mechanism. Dynamic microtubules explore the periphery by assembly and coupling to the retrogradely moving actin filaments via actin/microtubule linkers. Cross-section shows weak adhesion receptor coupling. (b) Adhesion/signaling: Growth cone makes contact with adhesive substrate, stimulating adhesion receptor clustering and signaling followed by preferential exploration of the adhesion site by microtubules that uncouple from the actin cytoskeleton. These microtubules could support signaling and receptor–cytoskeletal coupling. (c and d) Engorgement/Stretching/Traction/ Consolidation: Strong adhesion receptor cytoskeleton coupling results in actin flow attenuation and forward movement of actin recycling zone in the P domain, see inset. Actin arcs surrounding the C domain direct microtubules towards adhesion sites. Coordinated forward movement of C domain and T zone microtubules together with F-actin structures could be mediated by microtubule motors and actin/microtubule linkers. $M$ stands for any actin or microtubule based motor; $L$ stands for any actin/ microtubule linker. Time progresses from top to bottom. Image from~\cite{Suter2011}}
%	\label{fig:elongation}
%\end{figure}

\section{Elongation, steering and external stimuli}
Elongation is a key feature for neurites' outgrowth. The Growth Cone senses the environment during this process. Its ability to act depending on the local properties and the external stimuli, is both the most relevant ability of this biological micrometric walker and the main limit for the neuron's growth simulators.\\
The direction of elongation and its intensity allow or not the GC reaches the target and fulfil its biological aims. This ability of navigating onto the substrates and reach its target is widely studied in biology labs \footnote{\url{http://neurofluidics.org/program/  http://phdneurobiology.sissa.it/eng/faculty/laura-ballerini.aspx} \\ \url{http://phdneurobiology.sissa.it/eng/faculty/vincent-torre.aspx}}.

The desire of controlling elongation and enhancing the elongation process \textit{in vivo} is very ambitious and offers a wide spectrum of applications, mostly in medicine and rehabilitation.
Still, the mechanism of finding the axon path is really complex, with hundreds of chemical \cite{Wen2006} and mechanical \cite{Chua2014} cues interacting with a biological and active walker! Fortunately, the development of bio-mechanical technologies, as the AFM (atomic force microscope) or the optical tweezers, are offering a wider spectrum of measures. They are crucial to understanding this process and investigate the mechanism underlying the complex behaviour of axon and dendrites elongation.\\
We will show how the forces – intended as mechanical pressures and pulling – are getting a relevant place in biology and neuroscience and how the possibility of quantify them is changing models and approaches \cite{Athamneh2015}.
Oversimplifying the process, we can account for the elongation with the synthesis and fixation of tubulin in the micro-tubule formation.  The steering can be assumed as the direction where such microtubules grow longer, as in Figure \ref{fig:steering}.
Most diagrams of axonal elongation are, in essence, models of microtubule and actin dynamics occurring in the growth cone as it grows in response to extracellular cues. The debate is still open on which between the pulling of actin cytoskeleton and the pushing from the back plays the most significant role \cite{Rauch2013}.
However, this simple scheme hides most of the open questions in modern research on growth cone. In a recent article \cite{Kahn2016} the author wondered whether the microtubules play the main role in the invasion of the peripheral domain and in the consequent turning, or if the actin cytoskeleton is alone responsible of Growth Cone steering. This leaves us with open questions as:\newline

``What are the means by which each relevant molecular motor protein is activated or deactivated in a manner that contributes to the turning of the growth cone in response to environmental cues?''

The leading interpretation describes the process in the following manner. Polymerization of actin filaments at the periphery of the growth cone and central depolymerization effectively result in actin bundles moving backwards through the growth cone, giving the growth cone a caterpillar track style movement. The space that is created behind the growth cone by this pulling action is filled by microtubules and elongation occurs \cite{Kiddie2004}.

\begin{figure}
	\centering
\includegraphics[width=\linewidth]{images/turning}
	\caption{\textbf{Scheme of Growth Cone turning} The central domain of the growth cone is the microtubule (MT)-rich region contiguous with the axon shaft. The peripheral domain is the outer-most part of the growth cone. The peripheral domain comprises a broad flat lamellar region in which actin filaments are arranged as a meshwork, as well as elongated thin filopodia in which actin filaments are arranged as aligned bundles. The transition zone is the region between these two domains. Retrograde flow of the actin cytoskeleton in the peripheral domain pushes back most microtubules, compacting them in the central domain. Individual microtubules from the central domain are able to penetrate the transition zone to enter the peripheral domain during growth cone advance. (B) During growth cone turning, microtubules extend from the central domain through the transition zone preferentially in one side of the peripheral domain. Image from~\cite{Kahn2016}.}
	\label{fig:steering}
\end{figure}

The complex mechanism by which the flux of actin determines the synthesis of microtubule is too detailed for the scope of this work, the interested reader can refer to~\cite{Bornschlogl2013} which offers a purely mechanic model, synthesized in Fig.~\ref{fig:mechanic_retraction}.
\begin{figure}
	\centering
	\textbf{Mechanical model for Growth Cone elongation}
	\includegraphics[width=0.7\linewidth]{images/mechanic_force}
	\caption{Mechanical model for filopodia. Force production via the tip arises from the parallel action of membrane forces ($F_{mem}$) and from actin dynamics. Cortical retrograde flow ($v_{rc}$) couples with high friction to the filopodial actin shaft, modeled as a Kevin–Voigt body with an elastic modulus $k_{filo}$ and a viscosity $c_{filo}$. Polymerization at the tip ($v_{poly}$) slower than the cortical retrograde flow leads to retraction. Cytoskeletal force transduction can fail due to weak actin-membrane linkage at the tip ($b_{int}$). An elastic force with stiffness $k_{eff}$ controlled by the optical trap is exerted on the bead. Image from~\cite{Bornschlogl2013}.}
	\label{fig:mechanic_retraction}
\end{figure}
Following the excellent work of P. Recho in~\cite{Recho2016}, it is possible to distinguish whether they imply that the GC pulls the trailing axoplasm thanks to F-actin polymerisation pushing the membrane in the P-domain and myosin II contractility pulling from the T- domain or whether it is rather microtubules which, from the axoplasm, polymerise against the T-domain and propel the GC.
Even whit such basic knowledge on Growth Cone dynamics the relevance of interaction with environment come out. To study the reaction to mechanic or chemical stimuli leads to different research projects and questions, and the literature is very broad on the topic.

 Let's  now focus on understanding how the mechanical stimuli offered from a wall or from a generic stiff object will influence the behaviour of the growth cone. This subject is commonly designated as mechanical signalling. Even in this case the research is far from being concluded; the details how the different events are orchestrated to gradually build up traction force, up to hundreds of nanoNewton, and guide axonal growth in the direction of the adhesion site. In  \cite{Athamneh2015_Substrate}
the authors show that traction force increases gradually over time as the growth cone encounters a new adhesion substrate. The maximum level of the generated force depends on the stiffness of the new substrate, implying continuous strengthening of the clutch and/or active recruitment of molecular motors. From previous studies it is also known that the growth cone response to a physically restrained adhesion substrate includes adhesion formation, SRC-tyrosine phosphorylation, slowing of retrograde F-actin flow, increased actin assembly, advancing of microtubules, and leading edge advance.

\section{Growth cones in constrained environments}
Here we discuss some recent experiments performed to study the interactions between the active growth cone and the physical environment. We have seen that the variety of behaviours and stimuli is vast; therefore we will focus on the experiences closer to our laboratory and to the aims of NetGrowth.\\
\subsubsection{Experiment on environment interaction}
Let's now discuss some recent experiments performed to study the interactions between the active growth cone and the physical environment, we have seen the variety of behaviors and stimuli is spread so the attention will be focused on the experiences closer to our laboratory and to the aims of \texttt{NetGrowth}.\\
When talking of culture over substrates, and the consequence of, a net example of mechanically enhanced system is offered by the work of Ballerini's group at Sissa. They study cultures growing on a substrate of carbon nanotubes (CNT)~\cite{Sucapane2009}. Here, the presence of CNT  enhances the growth of neuronal tissue and accelerate the stabilization of synaptic cross-links up to the standard connectivity reached in control cultures.
In this case the signaling is proved to be not only mechanic but electric too. Some applications of this experimental setup were in a rehabilitation frame, helping the reconnection of bone ganglia.\\
The same group worked on 3D cultures built with transparent PDMS and studied the change the third dimension carries on
topology and geometry of the
network~\cite{Bosi2015}, an example of
2D and 3D cultures in Fig.~\ref{fig:23d}.
\begin{figure}
	\centering
	\textbf{From 2D to 3D: structural changes of \textit{in vitro} cultures}
	\includegraphics[width=0.7\linewidth]{images/23d}
	\caption{In (a) (top row) confocal micrographs show hippocampal cultures grown on 2D-PDMS (left) and 3D-PDMS
		(right) immune-stained for b-tubulin III (in red), GFAP (green) and DAPI (blue), scale bar is 100 mm. 3D platforms exhibits properties and behaviours that differ from their equivalent 2D configurations, neverthless neurons that grow on supports, such as multi-layered artificial substrates, are only in part exposed to the 3D environment, due to cell adhesion to the platform(s).
		It's stressed that 3D networks, besides displaying a similar degree of active cells when compared to 2D
		ones, display a higher rate of bursting, under both spontaneous and disinhibited conditions, due to an
		improved efficiency of neurons and neuronal connections in synchronizing their activity.\cite{Bosi2015}}
	\label{fig:23d}
\end{figure}

Another project we have collaborated with for the NetGrowth initiative is held by C. Villard and collaborators.\\
The group, located in the microfluidic lab of IPGG in Paris, is interested in measuring the modifications the mechanical
environment produces on Growth cone navigation. A first and seminal experiment was carried by R. Renaud, whom this
thesis dues very much. He has measured the affinity of growth cones with PDMS structure, revealing the formers' attitude
to follow the structure edges. Such
affinity has been evaluated
numerically as explained in
Fig.~\ref{fig:anglerenaud}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{images/angle_renaud}
	\caption{\textbf{Growth Cone affinity to PDMS structures} In this simple set of experiments R. Renaud studied the tendency of a Growth Cone to follow the edge of the PDMS structure when it suddenly turns of a certain angle. The growth cones are highlighted with a tubulin bonded green dye. To measure adequately the affinity and the percentage of adhesion is very hard with this setup, nevertheless it is possible to evidence a qualitative displacement from the structure when the angle is greater than $\pi$}
	\label{fig:anglerenaud}
\end{figure}
The group is also working on the diode project discussed in Fig.~\ref{fig:diode}, and in general on the modifications
that a narrow environment acts on the growth cone behavior.  They have implemented a selective “return to sender”
strategy by exploiting this phenomenon, redirecting axons growing in the unwanted “reverse” direction to their original
compartment via U-shaped lateral connections; and the efficiencies of different variations of this basic concept
regarding their direction selectivity, and demonstrated that they constitute a major improvement over the tapered
channels already used by the brain-on-a-chip community.  \\
In a recent work, Ian Audric, exposes the GC to a set of different shapes looking for common behaviors
and aiming to a comprehensive description of constrained growth. Unfortunately biology is hard to forecast and the
experiment ends up with a too poor evidence to asses any result. The author is working the project again and we hope
the NetGrowth simulator will benefice by his work in next months. A picture of the experimental set up is offered in
Fig.~\ref{fig:audric}.
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/audric}
        \caption{\textbf{Axons grow in tiny PDMS microchannels, from I. Audric experiment at IPGG.}
		(A) A growth cone advancing into narrow micro-channel.	Between time zero and 2h the GC (white arrow) retract itself and pause until a following GC joins it (red arrow). They keep on going forward without further pauses. Another cone is visible at time 16h (black arrow). (B ; C) Self-rolling phenomena in GC encountering an obstacle.}
	\label{fig:audric}
    \end{figure}[p]


\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{images/tunnels_renaud}
	\caption{\textbf{Experimental results on a new kind of axon diode}. \textbf{A} In this design, axons are expected to experience the junction differently depending on their directions.
		The red axon grows along the edge which undergoes continuous changes of orientation and makes a U-turn :
		this is the blocking direction.\\
		The blue axon continues straight as it can not fold around the corner, whose angle (in black) is smaller than the critical release angle : this is the forward direction.
		B- Experimentally, axons growing from the bottom to the top do preferentially go straight (left field) or make
		U-turns (right field) depending on the relative orientation of the junctions.}
	\label{fig:tunnelsrenaud}
\end{figure}

\begin{figure} \centering
	\includegraphics[width=0.7\linewidth]{images/diode}
	\caption{\textbf{A mechanical diode for neuronal cultures} Intrinsic direction selectivity in microfluidic chamber
		experiment. The bottom chamber is seeded with neurons, where they are left to grow. The affinity between Growth
		Cones and PDMS edges forces the axon to grow along the walls of the chamber, using this attitude, and the
		persistent direction of Growth Cone navigation it is possible to impose, mechanically, a preferred direction of
		outgrowing.\\
		The showes the correlation between the number of archs in the diode structure and the probability of reach the
		top chamber.  Image from~\cite{Renault2016}
	}
	\label{fig:diode}
\end{figure}

As I have remarked until now, the bibliography is really huge and I have done a big effort to summarize the relevant aspects, both in this relation than in the research work itself.\\ I hope the reader have a reduced but clear understanding of the neurite outgrowth and growth cone navigation to comprehend the next sections, where biology effectiveness is put aside for more affordable and computable models of neuronal outgrowth.

\section{From single GC to Neurite arborization, branching and competition}
Why do neurons branch? How do the branches relate to each other? Is there a main branch? Is branching similar in 2 and
3dimensions? Does exist, and which is the optimal configuration for the dendritic tree?%We can say for sure the answer is the simplest and at same time the one is leading all biological activity we have investigated until now: to increase the probability of reaching their biological targets.
All this question are very deep into the comprehension of biological mechanism of neuronal arborization. This field has
been highly investigated in the past and is nowadays, the understanding of neurite branching is far from be completed
and while some biological mechanism for the generation of new growth cones has been successfully reduced to their
minimal components the overall understanding of arborization process misses some relevant elements. To report the
evidences and offer a complete review of the models used to mimic neuronal branching will require a separate work.
Instead we will introduce the problem by the work of~\cite{Gallo2011} and \cite{Cuntz2010} \\.
The objective of nervous system is to produce a functional information process system. The units of this system are the neurons and neuronal population, how to connect each unit to others in an efficient way is the challenge the evolution had to solve.
The human brain cells solved the problem with the branching process: each neuron makes contacts with multiple targets through branching of its one axon, thereby reaching out to targets that are not in its direct trajectory, Fig.~\ref{fig:branching:objectives} presents an overview of the wiring problem
\begin{figure}[p]
	\centering
	\includegraphics[width=0.7\linewidth]{images/branching/objectives}
	\caption{\textbf{Modes of axon branching}. (\textbf{A}) A single neuron could make contacts with multiple targets by: (i) projecting a single axon, guiding it alternately to each target; (ii) project multiple axons, each axon being guided to an individual target, or possibly to multiple targets as in (i); (iii) project a single axon which undergoes branching and each branch contacts one or more targets. Natural selection has continuously solved the problem of neuronal connectivity by adopting the strategy of axon branching (iii). (\textbf{B}) Growth cone bifurcation begins by the loss of protrusive activity at the leading edge while maintaining lateral activity. This results in the formation of two independently active growth cones giving rise to two axon branches from the tip of the axon. In contrast, axon collateral branches form \textit{de novo} from the axon shaft after the growth cone has advanced past the site of branching. Collateral branches are initiated by the protrusion of filopodia or lamellipodia from the axon shaft. Image from \cite{Gallo2011}}.
	\label{fig:branching:objectives}
\end{figure}
\subsection{Evidences of branching \textit{in vitro}}
There are basically two ways of undergoes a branching process:
\begin{itemize}
	\item \textbf{Bifurcation of the growth cone at the tip of the axon} during axon extension.
	Growth cone bifurcation generates two axon branches from the tip of the extending axon. This process contributes to axon guidance and to the development of the basic organizational scheme of the nervous system. However, branching through growth cone bifurcation is not the major mechanism that contributes to the sculpting of axons in their target fields.
	\item \textbf{Formation of axon branches from the axon shaft} behind the advancing growth cone. Fig. \ref{fig:branching:objectives} \textbf{B}:
	Axon collateral branching – also referred to as interstitial branching – denotes the \textit{de novo} formation of an axon branch from the axon shaft, independently from the growth cone present at the distalmost segment of the axon. The formation of axon collateral branches is widely regarded as the major mechanism used to establish axon arbors in synaptic target fields.
\end{itemize}
We are not focusing on the complex biological machinery to generate a new growth cone. The main idea, however, is the
presence of a higher amount of Actin-F. Such excess can be caused by the growth cone stalling or the arrival of an
\textit{actin wave}. Once a protrusion is formed, the microtubules can invade the bulge and form the growth cone
structure we have discussed before. Fig. \ref{fig:actinmicrotubules} offers a schematic picture of the process.\\

To conclude this short overview about the biology of outgrowing neuron, it is necessary to discuss briefly the problem
of pruning. .
In order to elongate the GC has to assemble tubulin in microtubule, but the building blocks it disposes are generally
limited by the rate of synthesis in the soma~\cite{Goldberg2003}. Actually recent studies showed some protein are
produced in the neurite itself, but anyhow the law of cytoplasm conservation by Ramon and Cajal has to be respected
[Pestronk1995], establishing a competition among all extruding tips. Hence the neuron has to adopt strategies to
optimize its growth towards effective directions: exuberant axonal projections generated during development must be
differentially regulated so that beneficial branches are elongated while aberrant branches are
eliminated~\cite{Osan2011}. This large scale axon degeneration has been documented and studied in a variety of
developmental systems. Ollustrative examples of both branching and branch elimination are shown in Fig.~\ref{fig:pruning}.\\
\begin{figure}
	\centering
	\includegraphics[width=1\linewidth]{images/branching/actinmicrotubules}
	\caption{\textbf{Rise of a collateral axon} A basic sequence of the reorganization of the axonal cytoskeleton during branching. The first step involves the formation of actin-filament-based protrusions from the axon shaft. The protrusions are subsequently invaded by axonal microtubules, giving rise to a nascent branch. Maturation of the branch involves the entry of additional microtubules and cellular components into the nascent branch, e.g. organelles (not shown). \cite{Gallo2011}}
	\label{fig:actinmicrotubules}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/branching/pruning}\\
	\caption{\textbf{Time sequences showing branching and pruning of dissociated dorsal root ganglion neurites}. (a) Branching (red arrow) and extension (blue arrowheads) of primary axons. (b) Extension and retraction (blue arrowheads) of neurite tip. (c) Tertiary branching and pruning (encircled). ~\cite{Osan2011}}
	\label{fig:pruning}
\end{figure}

\subsection{Competition}
We have remarked the evolution shaped the genetics of neuron to fulfil their primary biological objective, create a
network of information processing units. To reach the post-synaptic neuron is the primary task of outgrowing axons, we
can consider pruning a method the neurons uses to optimize their neurites in this regard.
While the navigation should be considered a semi-local behavior -both the chemotaxic gradient and the mechanical confinement are sensed by the GC itself, the neurite influences the Growh Cone with the microtubule rigidity, and,
maybe, realising chemorepulsive signaling - branching and elongation are thought to involve the whole neurite or the
neuron even. Indeed the law of cytoplasm conservation is showed to be - almost - enough to shape the morphology of a dendritic
tree~\cite{Cuntz2010}. This constraint can be partially violated by the outgrowing dendrite, and these violations are
the reasons of the broad zoology of dendritic morphology.
A similar idea is sustained in the brilliant, thought old, review titled \textit{How does an axon
grow?}\cite{Goldberg2003}. Here the relevance of the question -Does the axon requires \textit{building blocks} from the soma to grow or it's self-sufficient?- is addressed and many experiments are reported to sustain the two hypothesis.
Beyond the complexity of cellular biology simple questions can be drawn -the newly created cellular membrane is
produced? Do the axon synthesize protein on its own or an diffusion/advective flux is received from the soma?-
Thus when simulating the growth cone shaping the dendrites we need to respect these general constraints and broaden our look at the whole neuron.
In the respect of these optimization principles we have settled from the point of view of the growth cones: they
compete each other for the limited resource the neuron disposes.
Some experiments were done to support these idea \cite{Hjorth2014,Miller2005}, and to find out which are the critical
nutrients that affect microtubule synthesis, and therefore, elongation.
Still is necessary to remark that the growth cone behavior is pretty variable and to make assertion on it requires a
huge amount of measures to obtain a result statistically valid. \\\\

In the next chapter the straight observation of neuronal behaviour leaves the way to a biophysical attitude for modelling, towards a simplified behaviour that can be reproduced with a computer.
\end{document}
