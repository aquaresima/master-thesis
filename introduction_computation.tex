% !TeX spellcheck = en_UK
% !TeX root = introduction_computation.tex
\documentclass[tesi.tex]{subfiles}

\begin{document}
\Chapter{Computation as an emergent property of networks}

\section{Neural or Neuronal Networks}
The very first concept to deal with is \textit{information}. When Shannon introduced the information theory in 1950
\cite{Shannon1948} he couldn't imagine it would have affected so much our contemporary culture. Indeed information is nowadays one of the most profitable and abused concept. Information is the amount of knowledge we have on a certain
system, the density measure of information, as introduced by Shannon, is its entropy.
The theory was formulated in the field of Communication Theory and aimed to formalize the transmission of discrete signals; in
this particular case, the entropy describe the uncertainty about the next character of the transmitted string.
The concept of information was soon related to \textit{computation}: the ability of process such
information, to extract relevant parts from the whole, detect similarities and make evaluations. Living beings exert
these actions all along their life.\\

The general quest about how the living matters process the information is on going debate, the discussion is even pushed
further to comprehend the nature of consciousness. Anyway a meeting point between Neuroscience and Informatics are
neural networks, an oriented graph of processing units. Neural Networks are driving
the Informatics research on Machine Learning and obtaining the most striking results in reproducing \textit{intelligent}
behaviour\footnote{The most famous was AlphaGo winning the world champion of Go, in 2016.}, attracting the attention of
researchers and stakeholders. \\

The common points between a formal
system, i.e. ANN, running on top of Silicon hardware and the soft
matter in our brains are the network structure and the existence of an activation threshold. A simple sketch of this parallel is offered in Fig~\ref{fig:neural_network}. One
of the most important aspects of this analogy is that \textit{Computation} appears not as an intrinsic property but
emerges from the web of connections among single, simple, units.

\begin{figure}[p]
    \centering
    \includegraphics[width = \textwidth]{introduction/neurons_network}
    \caption{\textbf{Neural or Neuronal
        Networks} The parallel between
        Artificial Neural Networks and
        biological NN can be sustained
        on two main evidences. First both the networks are built upon and with simple elements whose
computational properties are minimum, they can usually sum a set of input and confront with a threshold. The inputs can
be both excitatory or inhibitory.
Second, a common feature is the non linear response. The all or none behaviour is
defined by an activation function $\phi(x)$, which is a sigmoid for the artificial networks and variate by
neuron to neuron in biological tissue.
On the other end a relevant difference is the time domain the agents act on. For neuronal
networks the information appear to be
coded in time also, while the ANN
uses discrete, where each
evaluation takes one step. \textit{Thanks to
McCulloch \& Pitts youtube channel} }
\label{fig:neural_network}
\end{figure}

Computation, as every one knows, is the
fundamental feature of computers, which are composed by transistors, and in the very
end, by a well-structured amount of Silicon. The way these systems process the information is well known, studied and
improved day by day into researches lab.  Processors, complex structures of hardly more than nanometres components, are
among the most advanced product of human technology, they are composed basically by an ALU (Arithmetic Logic Unit) and a
CU (Control Unit) the former operates those actions we usually regard as computation while the latter control the first,
shift information and manage the whole process.\\
From a mathematical point of view, the work of Alan Turing (1912- 1954)
laid the foundations of computer science, and fixed the constraints to what machines can do or not\footnote{Turing introduced the notion of Turing complete machines, and showed the same, codified, computation
	can be performed on different architectures changing the particular set of procedures, the algorithms.
	Together with Gödel they set the limit of such machines by the very famous Halting Problem: for a undefined set of inputs a
	Turing machine can not say whether it's computation is going the end or not, these problems are said undecidable.
}. In 1980's the introduction of quantum computer changed profoundly the panorama, and new categories and tasks were
defined, changing completely the
panorama of complexity classes, and
redefining the concept of
\textit{hardness}. Maybe a better comprehension of biologically based information process will help to solve some of the most
puzzling questions of our century and
shade the light on the many doubts of
Infromatics.


\section{Graphs in Neuroscience}
The relevance of graph structures appear in various field of Neuroscience. The network framework provides a natural way
to describe neural organization. Indeed, information processing emerges from the activity of neural networks that carry
information from one cell assembly or brain region to another. The advent of Network Science suggests modifying the
traditional \textit{computer metaphor} for the brain to an \textit{Internet metaphor}, where the neocortex takes on the
task of \textit{packet switching}~\cite{Baronchelli2013}.
Network theory allows the shift from a reductionist to a \textit{complex system} view of brain organization. In this
framework, optimal brain functioning requires a balance between local processing and global
integration~\footnote{\url{http://www.scholarpedia.org/article/Integrated_information_theory}}. I will shortly present some examples:\\

\paragraph{Convergent or Divergent Connections} The very interesting work of Martin Lindauer~\cite{Lindauer} starts
from networks and synapses to reveal the complex simplicity of animals communication. In the account of sensory systems
Lindauer depicts the difference between divergent or convergent networks on the sensor nerve. When the signal from a
cell is shared with upper standing cells the information is reinforced and the possibility it arrives to higher areas of
the brain increases. It can also be processed and refined, as in the interneurons of eye's nervous system which convolve
many signals from the retina to obtain the \textit{contrasted} image. The convergent configuration, instead, allows for more
complex information: the perception is augmented mixing different signals, e.g. the bees antenna mixes olfactory and
tactile signals for a plastic perception of flavours. Fig~\ref{fig:Lindauer_connections} present a schema of the connections
types.

\begin{figure}[p]
    \begin{minipage}{0.6\textwidth}
    \includegraphics[width=\textwidth]{introduction/lindauer}
    \end{minipage}
    \begin{minipage}{0.3\textwidth}
    \includegraphics[width=\textwidth]{introduction/interazione}
    \end{minipage}
    \caption{\textbf{Network-Theory approach in Neuroscience} Network Theory offers an effective framework for the
        study of information processing properties of neuronal tissue. Mapping the connections among populations, or
        even among cells, into a schematic graph helps understanding some foundamental properties, as heterogeneity of
        connections, repeated patterns, node-node distance, etc\ldots. The pictures reproduce experimentally observed
        networks in two different scale of neuronal tissue.\\
        \textbf{Left picture}: scheme of nervous connection Lindauer observed in
        nervous systems of insects and mammalians. Sensor cells have two type of connections, usually both present in same
        sensory unit. The divergent connection ensure redundancy and signal transmission, while the convergent connections gives
        rise to complex stimulus when merged into an interneuron. On the right some examples of converging
        connections, each one with a specific function \cite{Lindauer}.\\
        \textbf{Right picture}:
        The cerebellum receives input from cortical areas via the pons and projects back to similar areas via the thalamus, forming a closed-loop
        architecture (black arrows). Complex cognitive processes such as language or social cognition require interactions
        between distributed regions in the cerebral cortex (colored broken arrows).\cite{Sokolov2017}}
    \label{fig:Lindauer_connections}
\end{figure}

\paragraph{Connectomics}
This field focuses on the production
and study of the
\textit{Connectomes}~\footnote{\url{http://www.scholarpedia.org/article/Connectome}}.
These are comprehensive maps of connections
within an organism's nervous system, typically its brain or eye.
The human connectome can be viewed as a
graph. Therefore, graph theory by its
rich tools, definitions, and
algorithms, is commonly used. By
comparing diseased connectomes and
healthy ones, it is possible to get insight into certain psychopathologies - such as
neuropathic pain - and potential therapies.
It is nowadays commonly accepted that
the cognitive abilities and the
information processing of brain's
tissue resides into the variate and
intense connection among brain areas. The connectome is characterized by short path lengths (a small-word topology),
high clustering, and assortativity, the tendency of hubs to be connected to hubs, forming a so‐called \textit{rich
club}, and an overlapping community structure~\cite{Baronchelli2013}.

\paragraph{Integrated Information Theory}
When talking about information processing and biological intrinsic properties, IIT plays the most distinguished role in the attempt to formulate a comprehensive theory of human consciousness. IIT lies on graph theory and precisely on the convergence between graph theory and Information theory. The topology of the
network is the most important aspect. Roughly speaking, the possible partitions represent the possible states of the
experience~\cite{Massimini}.

\section{Morphology and computing dendrites}
The importance of graph topology comes in action when moving to the single unit too. We have briefly mentioned the
simplicity of the  fundamental component of neuronal networks i.e. the nervous cell. However, the main attention of the work
in NetGrowth regards the complex topology and geometry of dendritic trees. The reader who is unfamiliar with neuron
biology will find a short review in Fig.~\ref{fig:neuron_biology}.
Hence, in the following paragraphs I
will present some examples of network.
Such systems due their properties to the
topology and morphology of networks
they are composed of.

\begin{figure}[h]
    \centering
    \includegraphics[width = \textwidth]{introduction/neuron_biology}
    \caption{\textbf{} \textbf{Scheme
        of neuronal structure} The
        picture presents a schematic
        neuron with all its fundamental components. The neurons communicate with each other throughout a electrochemical signal which is originated
    in the soma (cell-body) and travels across the axon up to the neurite of the post-synaptic neuron. The dendrites transport the
    signal towards the soma and play a significant role in amplifying, attenuating and preprocessing the signal. The
    synapses are the connection spots between axon and dendrites. About a million of synaptic
connections can arrange over a mature dendritic tree.}
    \label{fig:neuron_biology}
\end{figure}
A hint of the complex process of arborization in offered in the comprehensive review in~\cite{Cuntz2014}.
The dendrite is investigated as an active actor of the neuron-neuron signaling process, and its properties are outlined.
This work was a good basis over which to build our
simulator, and it is eventually to test the result we are obtaining.
The chapters we were most interested are those regarding the optimization principles the neurite satisfy during the
growth.
%We were not able until now to build generative models in this perspective but the next step for the simulator
%is towards these constraints.
The physical quantities which constraint the neurite
outgrowth are linked with its
biological limits and scope: the
maximization of neuron connectivity
given its limited set of time and
nutrients~\cite{Cuntz2010},\cite{Tsigankov2009}, e.g.\ the maximization of synaptic repertoire given the dendritic
arbor total length.\\

The properties of the dendritic tissue are relevant for signal transmission and processing also; Nonetheless the
early theories about dendritic tree, i.e. only a signal collector, it was suspected its role to be significantly
richer, in light of the very
typical dendritic architectures of some neuronal types. The presence of voltage-gated ion channels and action-potentials
inside the dendrites, nowadays observed, obliterates the idea that they merely funnel incoming signals passively to the soma.
Interestingly, it was shown that – under particular conditions – dendritic spikes can compensate for the
distance-dependent attenuation observed in passive dendrites, so that the efficacy of synapses is nearly
location-independent  \cite{Tobyj}. The function of active dendrites could therefore be a simple way for neurons to free
themselves from the strong metric constraints inherent to physical networks.  \\
But a neuron endowed with excitable
dendrites could do much more and perform a complex non-linear summation of its synaptic inputs, depending on their
spatial and temporal activation pattern. Although the importance of active dendritic trees has been predicted for a long time,
their role in computation has been experimentally observed quite recently. In the retina, for instance, direction
selectivity was shown to depend on active dendritic processing. Theoretical investigations showed that – under
certain circumstances – even neurons with passive, non excitable dendrites can compute non trivial
functions (like the XOR logic gate), so that dendritic (pre)processing might be a general feature throughout the nervous
system rather than an exception. \\
In practice it might not be evident to exploit such computing power in artificial devices because it would presumably
require precise positioning of synapses on the dendritic tree of each neuron, which is firstly technologically
challenging and secondly intrinsically regulated through non yet elucidated mechanisms.
The description of this problem lies outside the scope of the manuscript but underlies the importance of a plausible
reproduction  of neurite morphology. A short resume on well-known processing abilities is reported in Fig.~\ref{fig:computing_units}.
\begin{figure}
	\centering
        \includegraphics[width =0.7\textwidth]{introduction/computing_units}
	\caption{\textbf{The dendritic computational toolkit} A schematic figure highlighting four key dendritic mechanisms,
		mapped onto a layer 5 pyramidal neuron morphology, which can allow dendrites to act as computational elements.
		These mechanisms can coexist in the same neuron and be active in parallel or in a hierarchical manner.\\
		\textit{Bottom left}: Passive dendrites act as passive filters. A high-frequency fluctuating current injected in
		the dendritic pipette will evoke high-frequency and large-amplitude local voltage responses, but the response
		recorded by the somatic pipette will be attenuated and smoothed (low pass filtered).\\ \textit{Top left}:
		Nonlinear interaction between excitation and shunting inhibition on small dendritic branches can implement
		logical operations. The branch marked by an arrow sums up the current from the two subtrees, such that its
		output would be a logical OR on their output. Each of the subtrees will in turn inject current if and only if
		the excitation AND-NOT the inhibition will be active.\\  \textit{Bottom right}: Dendrites can help reduce or
		amplify the mutual interaction between excitatory inputs. Excitatory inputs to the same branch tend to sum
		sublinearly, whereas inputs on different branches sum linearly. Thus mapping input to different branches can
		reduce this effect. In neurons with active dendrites, however, clusters of inputs active synchronously on the
		same branch can evoke a local dendritic spike, which leads to significant amplification of the input. Synapses
		onto a different branch (open circles) are only slightly influenced by this spike.\\  \textit{Top right}: In layer
		5 cortical pyramidal neurons, as depicted here, coincidence detection between the apical and basal dendritic
		compartments is achieved by active dendritic mechanisms. A backpropagating action potential, which coincides
		with a distal synaptic input, will trigger a dendritic Ca 2+ spike, which depolarizes the whole apical dendrite and
		drives a burst of spikes in the
		axon.\cite{dendritic_computation} }
	\label{fig:computing_units}
\end{figure}


\end{document}
